﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;


namespace agroBJX.Sessiones
{

    public static class Cls_Sessiones
    {
        private static String S_Empleado_ID = "Empleado_ID";
        private static String S_Nombre_Usuario = "Nombre_Usuario";
        private static String S_Nombre_Empresa = "Nombre_Empresa";
        private static String S_No_Usuario = "No_Usuario";
        private static String S_Rol_ID = "Rol_ID";
        private static String S_Nombre_Rol = "Nombre_Rol";
        private static String S_Mostrar_Menu = "Mostrar_Menu";
        private static String S_Dependencia_ID_Empleado = "Dependencia_ID_Empleado";
        private static String S_Area_ID_Empleado = "Area_ID_Empleado";
        private static String S_Datos_Empleado = "Datos_Empleado";
        private static String S_Totales = "Totales_Percepciones_Deducciones";
        private static String S_Historial_Nomina_Generada = "Nomina_Generada";
        private static String S_Menus_Control_Acceso = "MENUS_CONTROL_ACCESO";
        private static String S_Dt_Proveedor = "Dt_Proveedor";
        private static String S_Forma_Cambios = "Forma_Cambios";
        private static String S_Dt_Conceptos_Cobro_Facturacion = "Dt_Conceptos_Cobro_Facturacion";
        private static String S_Dt_Predios_Facturacion = "Dt_Predios_Facturacion";
        private static String S_Dt_Fechas_Pagos_Estado_Cuenta = "Dt_Fechas_Pagos_Estado_Cuenta";
        private static String S_Dt_Cuentas_Asignadas_Analista = "Dt_Cuentas_Asignadas_Analista";
        private static String S_Dt_Reporte_Dictamenes = "Dt_Reporte_Dictamenes";
        private static String S_Dt_Cuentas_Asignadas_Guardadas = "Dt_Cuentas_Asignadas_Guardadas";
        private static String S_Dt_Reporte_Facturado_Detalles = "Dt_Reporte_Facturado_Detalles";
        private static String S_Dt_Reporte_Contratos_Cuentas_Detalles = "Dt_Reporte_Contratos_Cuentas_Detalles";
        private static String S_Dependencia_Empleado = "S_Dependencia_Empleado";

        private static String S_Lecturas_registradas = "lecturas_registradas";
        private static String S_Lecturas_no_registradas = "lecturas_no_registradas";
        private static String S_Lecturas_actualizadas = "lecturas_actualizadas";
        private static String S_Cierre_OT = "Cierre_OT";


        public static bool Cierre_OT
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Cierre_OT] == null)
                    return false;
                else
                    return Convert.ToBoolean(HttpContext.Current.Session[Cls_Sessiones.S_Cierre_OT]);
            }
            set 
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Cierre_OT] = value;
            }
        }

        public static String lecturasNoRegistradas
        {

            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_no_registradas] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_no_registradas].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_no_registradas] = value;
            }
        
        }

        public static String lecturasActualizadas
        {

            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_actualizadas] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_actualizadas].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_actualizadas] = value;
            }

        }

        public static String lecturasRegistradas
        {
            get {

                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_registradas] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_registradas].ToString();
            }
            set {
                HttpContext.Current.Session[Cls_Sessiones.S_Lecturas_registradas] = value;
            }
        
        }


        public static String Empleado_ID
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Empleado_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Empleado_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Empleado_ID] = value;
            }
        }
        public static String Nombre_Usuario
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Usuario] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Usuario].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Usuario] = value;
            }
        }
        public static String Nombre_Empresa
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Empresa] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Empresa].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Empresa] = value;
            }
        }
        public static String No_Usuario
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_No_Usuario] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_No_Usuario].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_No_Usuario] = value;
            }
        }


        public static String Rol_ID
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Rol_ID] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Rol_ID].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Rol_ID] = value;
            }
        }

        public static String Nombre_Rol
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Rol] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Rol].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Nombre_Rol] = value;
            }
        }
        public static bool Mostrar_Menu
        {
            get
            {
                bool dato = Convert.ToBoolean(HttpContext.Current.Session[Cls_Sessiones.S_Mostrar_Menu]);
                return dato;
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Mostrar_Menu] = value;
            }
        }

        public static String Dependencia_ID_Empleado
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_ID_Empleado] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_ID_Empleado].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_ID_Empleado] = value;
            }
        }

        public static String Dependencia_Empleado
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_Empleado] == null)
                    return string.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_Empleado].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dependencia_Empleado] = value;
            }
        }

        public static String Area_ID_Empleado
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Area_ID_Empleado] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Area_ID_Empleado].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Area_ID_Empleado] = value;
            }
        }
        public static DataTable Datos_Empleado
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Datos_Empleado] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Datos_Empleado];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Datos_Empleado] = value;
            }
        }

        public static DataTable Totales_Percepciones_Deducciones
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Totales] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Totales];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Totales] = value;
            }
        }

        public static StringBuilder Historial_Nomina_Generada
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Historial_Nomina_Generada] == null)
                    return null;
                else
                    return (StringBuilder)HttpContext.Current.Session[Cls_Sessiones.S_Historial_Nomina_Generada];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Historial_Nomina_Generada] = value;
            }
        }


        public static DataTable Menu_Control_Acceso
        {
            get
            {
                if (HttpContext.Current.Session[Cls_Sessiones.S_Menus_Control_Acceso] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Menus_Control_Acceso];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Menus_Control_Acceso] = value;
            }
        }
        public static DataTable Datos_Proveedor
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Proveedor] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Proveedor];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Proveedor] = value;
            }
        }
        public static String Forma_Cambios
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Forma_Cambios] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session[Cls_Sessiones.S_Forma_Cambios].ToString();
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Forma_Cambios] = value;
            }
        }

        public static DataTable Dt_Conceptos_Cobro_Facturacion
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Conceptos_Cobro_Facturacion] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Conceptos_Cobro_Facturacion];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Conceptos_Cobro_Facturacion] = value;
            }
        }

        public static DataTable Dt_Predios_Facturacion
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Predios_Facturacion] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Predios_Facturacion];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Predios_Facturacion] = value;
            }
        }

        public static DataTable Dt_Fechas_Pagos_Estado_Cuenta
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Fechas_Pagos_Estado_Cuenta] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Fechas_Pagos_Estado_Cuenta];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Fechas_Pagos_Estado_Cuenta] = value;
            }
        }

        public static DataTable Dt_Cuentas_Asignadas_Analista
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Cuentas_Asignadas_Analista] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Cuentas_Asignadas_Analista];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Cuentas_Asignadas_Analista] = value;
            }
        }

        public static DataTable Dt_Reporte_Dictamenes
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Dictamenes] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Dictamenes];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Dictamenes] = value;
            }
        }

        public static DataTable Dt_Cuentas_Asignadas_Guardadas
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Cuentas_Asignadas_Guardadas] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Cuentas_Asignadas_Guardadas];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Cuentas_Asignadas_Guardadas] = value;
            }
        }

        public static DataTable Dt_Reporte_Facturado_Detalles
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Facturado_Detalles] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Facturado_Detalles];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Facturado_Detalles] = value;
            }
        }
        public static DataTable Dt_Reporte_Contratos_Cuentas_Detalles
        {
            get
            {
                // Verifica si es null
                if (HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Contratos_Cuentas_Detalles] == null)
                    return null;
                else
                    return (DataTable)HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Contratos_Cuentas_Detalles];
            }
            set
            {
                HttpContext.Current.Session[Cls_Sessiones.S_Dt_Reporte_Contratos_Cuentas_Detalles] = value;
            }
        }
    }
}
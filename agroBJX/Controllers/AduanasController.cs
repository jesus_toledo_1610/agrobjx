﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class AduanasController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Aduana ControladorAduanas;

        public AduanasController()
        {
            ControladorAduanas = new Aduana();
        }

        [Route("api/aduanas")]
        [HttpPost]
        public string aduanas( Models.Aduana Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Aduana ControladorAduanas = new Models.Aduana();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorAduanas = Datos;
                ControladorAduanas.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorAduanas.Fecha_Registro = "GETDATE()";
                if (ControladorAduanas.Captura.Equals("I"))
                {
                    ControladorAduanas.Aduana_ID = int.Parse(ControladorAduanas.ConsultMaximoAduana().Rows[0]["Aduana_ID"].ToString()).ToString("00000");
                    
                        if (ControladorAduanas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorAduanas.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultaraduana/{id}")]
        [HttpGet]
  

        public string consultaraduana(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorAduanas.Aduana_ID = usuario;
            Dt_Data = ControladorAduanas.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        [Route("api/listaaduanas")]
        [HttpGet]
        public string listaaduanas()
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2ListaAduana> list = new List<Select2ListaAduana>();
            try
            {
                ControladorAduanas.Estatus = "ACTIVO";
                Dt_Registros = ControladorAduanas.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2ListaAduana
                    {
                        id = row.Field<String>("Aduana_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                        contacto = row.Field<String>("Contacto").ToString().Trim(),
                        telefono = row.Field<String>("Telefono").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }


    }
}

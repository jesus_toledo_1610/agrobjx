﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class ClientesController : ApiController
    {

        agroBJXEntities db = new agroBJXEntities();
        private Cliente ControladorClientes;

        public ClientesController()
        {

            ControladorClientes = new Cliente();
            
        }

        [Route("api/clientes")]
        [HttpPost]
        public string clientes( Models.Cliente Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Cliente ControladorClientes = new Models.Cliente();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorClientes =Datos;
                ControladorClientes.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorClientes.Fecha_Registro = "GETDATE()";
                if (ControladorClientes.Captura.Equals("I"))
                {
                    ControladorClientes.Cliente_ID = int.Parse(ControladorClientes.ConsultMaximoCliente().Rows[0]["Cliente_ID"].ToString()).ToString("00000");
                    
                        if (ControladorClientes.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorClientes.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultarcliente/{id}")]
        [HttpGet]
  

        public string consultarcliente(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorClientes.Cliente_ID = usuario;
            Dt_Data = ControladorClientes.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        [Route("api/listaclientes")]
        [HttpGet]
        public string listaclientes()
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2ListaCliente> list = new List<Select2ListaCliente>();
            try
            {
                ControladorClientes.Estatus = "ACTIVO";
                Dt_Registros = ControladorClientes.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2ListaCliente
                    {
                        id = row.Field<String>("Cliente_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                        razon = row.Field<String>("Razon_Social").ToString().Trim(),
                        rfc = row.Field<String>("RFC").ToString().Trim(),
                        ubicacion = row.Field<String>("Ubicacion").ToString().Trim(),
                        contacto = row.Field<String>("Nombre").ToString().Trim(),
                        correo = row.Field<String>("Email").ToString().Trim(),
                        telefono = row.Field<String>("Telefono").ToString().Trim(),
                        
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }

    }
}

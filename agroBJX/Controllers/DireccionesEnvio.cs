﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class DireccionesEnvioController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private DireccionEnvio ControladorDirecciones;

        public DireccionesEnvioController()
        {
            ControladorDirecciones = new DireccionEnvio();
        }

        [Route("api/direcciones")]
        [HttpPost]
        public string direcciones( Models.DireccionEnvio Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.DireccionEnvio ControladorDirecciones = new Models.DireccionEnvio();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorDirecciones = Datos;
                ControladorDirecciones.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorDirecciones.Fecha_Registro = "GETDATE()";
                if (ControladorDirecciones.Captura.Equals("I"))
                {
                    ControladorDirecciones.Direccion_Envio_ID = int.Parse(ControladorDirecciones.ConsultMaximo().Rows[0]["Direccion_Envio_ID"].ToString()).ToString("00000");
                    
                        if (ControladorDirecciones.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorDirecciones.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultardireccion/{id}")]
        [HttpGet]
  

        public string consultardireccion(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorDirecciones.Direccion_Envio_ID = usuario;
            Dt_Data = ControladorDirecciones.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }


        [Route("api/listadirecciones/{id}")]
        [HttpGet]
        public string listadirecciones(int id)
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2ListaDirecciones> list = new List<Select2ListaDirecciones>();
            try
            {
                ControladorDirecciones.Estatus = "ACTIVO";
                string cliente = String.Format("{0:00000}", id);
                ControladorDirecciones.Cliente_ID = cliente;
                Dt_Registros = ControladorDirecciones.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2ListaDirecciones
                    {
                        id = row.Field<String>("Direccion_Envio_ID").ToString(),
                        text = row.Field<String>("Direccion_Envio").ToString().Trim(),
                        ciudad = row.Field<String>("Ciudad").ToString().Trim(),
                        colonia = row.Field<String>("Colonia").ToString().Trim(),
                        cp = row.Field<String>("CP").ToString().Trim(),
                        estado = row.Field<String>("Estado").ToString().Trim(),
                        pais = row.Field<String>("Pais").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }


        [Route("api/listadireccion")]
        [HttpGet]
        public string listadireccion()
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorDirecciones.Estatus = "ACTIVO";
                Dt_Registros = ControladorDirecciones.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<String>("Direccion_Envio_ID").ToString(),
                        text = row.Field<String>("Direccion_Envio").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }


    }
}

﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class FleterasController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Fletera ControladorFleteras;

        public FleterasController()
        {
            ControladorFleteras = new Fletera();
        }

        [Route("api/fleteras")]
        [HttpPost]
        public string fleteras( Models.Fletera Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Fletera ControladorFleteras = new Models.Fletera();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorFleteras = Datos;
                ControladorFleteras.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorFleteras.Fecha_Registro = "GETDATE()";
                if (ControladorFleteras.Captura.Equals("I"))
                {
                    ControladorFleteras.Fletera_ID = int.Parse(ControladorFleteras.ConsultMaximoFletera().Rows[0]["Fletera_ID"].ToString()).ToString("00000");
                    
                        if (ControladorFleteras.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorFleteras.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultarfletera/{id}")]
        [HttpGet]
  

        public string consultarfletera(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorFleteras.Fletera_ID = usuario;
            Dt_Data = ControladorFleteras.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }


        [Route("api/listafleteras")]
        [HttpGet]
        public string listafleteras()
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2ListaFleteras> list = new List<Select2ListaFleteras>();
            try
            {
                ControladorFleteras.Estatus = "ACTIVO";
                Dt_Registros = ControladorFleteras.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2ListaFleteras
                    {
                        id = row.Field<String>("Fletera_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                        contacto = row.Field<String>("Contacto").ToString().Trim(),
                        telefono = row.Field<String>("Telefono").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }

    }
}

﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class InsumosController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Insumo ControladorInsumo;

        public InsumosController()
        {
            ControladorInsumo = new Insumo();
        }

        [Route("api/insumos")]
        [HttpPost]
        public string insumos( Models.Insumo Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Insumo ControladorInsumo = new Models.Insumo();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorInsumo = Datos;
                ControladorInsumo.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorInsumo.Fecha_Registro = "GETDATE()";
                if (ControladorInsumo.Captura.Equals("I"))
                {
                    ControladorInsumo.Insumo_ID = int.Parse(ControladorInsumo.ConsultMaximo().Rows[0]["Insumo_ID"].ToString()).ToString("00000");
                    
                        if (ControladorInsumo.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorInsumo.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultarinsumo/{id}")]
        [HttpGet]
  

        public string consultarinsumo(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorInsumo.Insumo_ID = usuario;
            Dt_Data = ControladorInsumo.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

    }
}

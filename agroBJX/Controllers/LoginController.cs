﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;

namespace agroBJX.Controllers
{
    public class LoginController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Usuario ControladorUsuario;
        [Route("api/Salir")]
        [HttpPost]
        public void Salir()
        {
            Cls_Sessiones.Nombre_Usuario = null;
            Cls_Sessiones.No_Usuario = null;
            FormsAuthentication.SignOut();
        }
        public LoginController() {
            ControladorUsuario = new Usuario();
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        [Route("api/Acceso")]

        public string Acceso(Models.Login Datos)
        {
            
            DataTable Dtregistro = new DataTable();
            String respuesta = "No hay respuesta del sevidor";
            Cat_Usuarios usuario = new Cat_Usuarios();
            
            try
            {
                if (Datos == null)
                    return "No es correcta la petición";
                ControladorUsuario.Email = Seguridad.Encriptar(Datos.email);
                ControladorUsuario.Password = Seguridad.Encriptar(Datos.password);
                Dtregistro = ControladorUsuario.Consult();

                if (Dtregistro.Rows.Count>0 && Dtregistro!=null)
                {
                    respuesta = "Bienvenido";
                           String Estatus = Dtregistro.Rows[0]["Estatus"].ToString();
                    if (Estatus == "ACTIVO")
                    {
                        Cls_Sessiones.Nombre_Usuario = Dtregistro.Rows[0]["Nombre"].ToString() + " " + Dtregistro.Rows[0]["Apellido_Paterno"].ToString() + " " + Dtregistro.Rows[0]["Apellido_Materno"].ToString();
                        Cls_Sessiones.No_Usuario = Dtregistro.Rows[0]["Usuario_ID"].ToString();
                    }
                    else
                        respuesta = "El usuario no se encuentra activo en el sistema";

                }
                else
                    respuesta = "Correo o Contraseña invalido";
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.InnerException);
                respuesta = e.Message;
            }
            return respuesta;
        }


        [Route("api/ejemplo")]
        [HttpPost]
        public string prueba()
        {
            return "ejemplo exitoso";
        }
    }
}

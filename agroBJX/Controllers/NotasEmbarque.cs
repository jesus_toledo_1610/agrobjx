﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace agroBJX.Controllers
{
    public class NotasEmbarqueController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private NotaEmbarque ControladorNotas;

        public NotasEmbarqueController()
        {
            ControladorNotas = new NotaEmbarque();
        }

        [Route("api/notas")]
        [HttpPost]
        public string notas( Models.NotaEmbarque Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.NotaEmbarque ControladorAduanas = new Models.NotaEmbarque();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorNotas = Datos;
                ControladorNotas.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorNotas.Fecha_Registro = "GETDATE()";
                if (ControladorNotas.Captura.Equals("I"))
                {
                    ControladorNotas.Embarque_ID = int.Parse(ControladorAduanas.ConsultMaximo().Rows[0]["Embarque_ID"].ToString()).ToString("00000");
                    
                        if (ControladorNotas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                        generarpdf(ControladorNotas.Embarque_ID);
                        string nombre = "Nota" + ControladorNotas.Embarque_ID + ".pdf";
                        respuesta = nombre;
                        //respuesta = "Registro realizado correctamente.";
                    }
                    

                }
                else
                {
                    if (ControladorNotas.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }


        public string generarpdf(string id) {
            DataTable dt = new DataTable();
            ControladorNotas.Embarque_ID = id;
            dt=ControladorNotas.Consult();
           string contents = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/Confirmacion.html"));
            string dir = System.AppDomain.CurrentDomain.BaseDirectory + "Temporal\\";
            string nombre = "Nota" + id + ".pdf";
            string ruta = dir + nombre;
            FileStream fs = new FileStream(ruta, FileMode.Create);
            Document docl = new Document(iTextSharp.text.PageSize.LETTER, 20, 4, 10, 10);
            Document doc = new Document(PageSize.LETTER);

            PdfWriter pw = PdfWriter.GetInstance(doc, fs);
            doc.SetMargins(20, 35, 20, 35);
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1250, BaseFont.EMBEDDED);
            Font text = new Font(bf, 12f, Font.BOLD, BaseColor.BLACK);

            BaseFont bf2 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
            Font text2 = new Font(bf, 10f, Font.NORMAL, BaseColor.BLACK);
            pw.PageEvent = new HeaderFooter();
            //seccion encabezado
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(Path.Combine("http://162.214.78.15/Agro_Bajio/assets/images/logo.jpeg"));
            logo.SetAbsolutePosition(0f, 0f);
            logo.ScaleAbsolute(90f, 90f);

            
            var table = new PdfPTable(3) { WidthPercentage = 100 };
            table.AddCell(new PdfPCell(logo) { Border = 0, BorderWidthBottom = 0, BackgroundColor = new BaseColor(13, 69, 95), BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            table.AddCell(new PdfPCell(new Phrase("AGROBJX AGROPRODUCCION INTEGRAL DEL BAJIO")) { Border = 0, BorderWidthBottom = 0, BackgroundColor = new BaseColor(13, 69, 95), BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_CENTER });
            table.AddCell(new PdfPCell(new Phrase("")) { Border = 0, BorderWidthBottom = 0, BorderWidthTop = 0, BackgroundColor = new BaseColor(13, 69, 95), HorizontalAlignment = Element.ALIGN_MIDDLE });
            
            var table2 = new PdfPTable(3) { WidthPercentage = 100 };
            table2.AddCell(new PdfPCell(new Phrase("")) { Border = 1, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            table2.AddCell(new PdfPCell(new Phrase("DATOS DEL EMBARQUE")) { Border = 1, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_CENTER });
            table2.AddCell(new PdfPCell(new Phrase("")) { Border = 1, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_MIDDLE });
            
            var table4 = new PdfPTable(3) { WidthPercentage = 100 };
            table4.AddCell(new PdfPCell(new Phrase("Cliente: " + dt.Rows[0]["Cliente"] ,text2) ) { Border = 1, BorderWidthRight = 0, BorderWidthLeft=1, BorderWidthBottom = 0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_LEFT, PaddingTop = 10 });
            table4.AddCell(new PdfPCell(new Phrase("")) { Border = 0, BorderWidthRight = 0, BorderWidthBottom = 0, BorderWidthTop=1, HorizontalAlignment = Element.ALIGN_RIGHT });
            table4.AddCell(new PdfPCell(new Phrase("Folio Venta: " + dt.Rows[0]["Embarque_ID"], text2)) { Border = 1, BorderWidthRight = 1, BorderWidthBottom = 0, BorderWidthLeft=0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_CENTER });

            var table5 = new PdfPTable(3) { WidthPercentage = 100 };
            table5.AddCell(new PdfPCell(new Phrase("Fecha Embarque: " + DateTime.Parse(dt.Rows[0]["Fecha_Embarque"].ToString()).ToString("dd/MM/yyyy"),text2)) { Border = 1, BorderWidthRight = 0, BorderWidthTop = 0, BorderWidthLeft=1, BorderWidthBottom = 0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_LEFT,PaddingTop=10,PaddingBottom=10 });
            table5.AddCell(new PdfPCell(new Phrase("")) { Border = 0, BorderWidthRight = 0, BorderWidthBottom = 0, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT });
            table5.AddCell(new PdfPCell(new Phrase(" " )) { Border = 1, BorderWidthRight = 1, BorderWidthBottom = 0,BorderWidthLeft=0,BorderWidthTop=0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_RIGHT });

            var table6 = new PdfPTable(3) { WidthPercentage = 100 };
            table6.AddCell(new PdfPCell(new Phrase("Fletera: " + dt.Rows[0]["Fletera"], text2)) { Border = 1, BorderWidthRight = 0, BorderWidthTop = 0, BorderWidthLeft = 1, BorderWidthBottom = 0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_LEFT, PaddingTop = 10, PaddingBottom = 10 });
            table6.AddCell(new PdfPCell(new Phrase("")) { Border = 0, BorderWidthRight = 0, BorderWidthBottom = 0, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT });
            table6.AddCell(new PdfPCell(new Phrase(" ")) { Border = 1, BorderWidthRight = 1, BorderWidthBottom = 0, BorderWidthLeft = 0, BorderWidthTop = 0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_RIGHT });

            var table7 = new PdfPTable(3) { WidthPercentage = 100 };
            table7.AddCell(new PdfPCell(new Phrase("Aduana: " + dt.Rows[0]["Aduana"], text2)) { Border = 1, BorderWidthRight = 0, BorderWidthTop = 0, BorderWidthLeft = 1, BorderWidthBottom = 1, Rowspan = 2, HorizontalAlignment = Element.ALIGN_LEFT, PaddingTop = 10, PaddingBottom = 10 });
            table7.AddCell(new PdfPCell(new Phrase("")) { Border = 0, BorderWidthRight = 0, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_RIGHT });
            table7.AddCell(new PdfPCell(new Phrase(" ")) { Border = 1, BorderWidthRight = 1, BorderWidthBottom = 1, BorderWidthLeft = 0, BorderWidthTop = 0, Rowspan = 2, HorizontalAlignment = Element.ALIGN_RIGHT });

            var table8 = new PdfPTable(3) { WidthPercentage = 100 };
            table8.AddCell(new PdfPCell(new Phrase("")) { Border = 1, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            table8.AddCell(new PdfPCell(new Phrase("DETALLES DEL EMBARQUE")) { Border = 1, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_CENTER });
            table8.AddCell(new PdfPCell(new Phrase("")) { Border = 1, BorderWidthBottom = 1, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_MIDDLE });

            var table9 = new PdfPTable(4) { WidthPercentage = 100 };
            table9.AddCell("Clave Producto");
            table9.AddCell("Producto");
            table9.AddCell("Cantidad");
            table9.AddCell("Precio Unitario");

            var table10 = new PdfPTable(4) { WidthPercentage = 100 };
            table10.AddCell(""+dt.Rows[0]["Clave_Producto"]);
            table10.AddCell("" + dt.Rows[0]["Producto"]);
            table10.AddCell(new PdfPCell(new Phrase("" + dt.Rows[0]["Numero_Cajas"])) { HorizontalAlignment = Element.ALIGN_RIGHT });
            table10.AddCell(new PdfPCell(new Phrase("" + dt.Rows[0]["Precio_Unitario"])) { HorizontalAlignment = Element.ALIGN_RIGHT });

            var table11 = new PdfPTable(4) { WidthPercentage = 100 };
            table11.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table11.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table11.AddCell(new PdfPCell(new Phrase("Precio Fletera", text2)));
            table11.AddCell(new PdfPCell(new Phrase("" + dt.Rows[0]["Precio_Fletera"])) { HorizontalAlignment = Element.ALIGN_RIGHT });

            var table13 = new PdfPTable(4) { WidthPercentage = 100 };
            table13.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table13.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table13.AddCell(new PdfPCell(new Phrase("Precio_Aduana", text2)));
            table13.AddCell(new PdfPCell(new Phrase("" + dt.Rows[0]["Precio_Aduana"])) { HorizontalAlignment = Element.ALIGN_RIGHT });

            var table14 = new PdfPTable(4) { WidthPercentage = 100 };
            table14.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table14.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table14.AddCell(new PdfPCell(new Phrase("SubTotal", text2)));
            table14.AddCell(new PdfPCell(new Phrase("" + dt.Rows[0]["Sub_Total"])) { HorizontalAlignment = Element.ALIGN_RIGHT });

            var table12 = new PdfPTable(4) { WidthPercentage = 100 };
            table12.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table12.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
            table12.AddCell(new PdfPCell(new Phrase("Total",text2)));
            table12.AddCell(new PdfPCell(new Phrase(""+ dt.Rows[0]["Total"])) { HorizontalAlignment=Element.ALIGN_RIGHT });
            

            doc.Open();
            doc.Add(new Phrase(""));
            doc.Add(new Phrase(""));
            doc.Add(Chunk.NEWLINE);
            doc.Add(table);
            doc.Add(new Phrase(""));
            doc.Add(Chunk.NEWLINE);
            doc.Add(table2);;
            doc.Add(new Phrase(""));
            doc.Add(Chunk.NEWLINE);
            doc.Add(table4);
            doc.Add(new Phrase(""));
            doc.Add(table5);
            doc.Add(new Phrase(""));
            doc.Add(table6);
            doc.Add(new Phrase(""));
            doc.Add(table7);
            doc.Add(new Phrase(""));
            doc.Add(Chunk.NEWLINE);
            doc.Add(table8);
            doc.Add(new Phrase(""));
            doc.Add(Chunk.NEWLINE);
            doc.Add(table9);
            doc.Add(new Phrase(""));
            doc.Add(table10);
            doc.Add(new Phrase(""));
            doc.Add(table11);
            doc.Add(new Phrase(""));
            doc.Add(new Phrase(""));
            doc.Add(table13);
            doc.Add(new Phrase(""));
            doc.Add(table14);
            doc.Add(new Phrase(""));
            doc.Add(table12);
            doc.Close();
            return nombre;
        }

        class HeaderFooter : PdfPageEventHelper
        {
            public override void OnEndPage(PdfWriter writer, Document document)
            {
                //base.OnEndPage(writer, document);
                PdfContentByte cb = writer.DirectContentUnder;
                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(Path.Combine("http://162.214.78.15/Agro_Bajio/assets/images/logo.jpeg"));
                img.SetAbsolutePosition(50f, 50f);
                PdfGState state = new PdfGState();
                state.FillOpacity = 0.3f;
                cb.SetGState(state);
                cb.AddImage(img);
                PdfPTable tb = new PdfPTable(3);
                tb.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                tb.DefaultCell.Border = 0;
                tb.AddCell(new Paragraph());
                PdfPCell _cell = new PdfPCell(new Paragraph("Pagina " + writer.PageNumber + " de " + writer.PageNumber)) { Border = 0, BorderWidthBottom = 0, BorderWidthLeft = 0, BorderWidthRight = 0, BorderWidthTop = 0, HorizontalAlignment = Element.ALIGN_CENTER };
                tb.AddCell(_cell);
                tb.AddCell(new Paragraph());
                tb.WriteSelectedRows(0, -1, document.LeftMargin, writer.PageSize.GetBottom(document.BottomMargin) - 5, writer.DirectContent);
            }
        }

        [Route("api/consultarembarque/{id}")]
        [HttpGet]
  

        public string consultarembarque(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorNotas.Embarque_ID = usuario;
            Dt_Data = ControladorNotas.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }



    }
}

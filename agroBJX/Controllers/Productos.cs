﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class ProductosController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Producto ControladorProductos;

        public ProductosController()
        {
            ControladorProductos = new Producto();
        }

        [Route("api/productos")]
        [HttpPost]
        public string productos( Models.Producto Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Producto ControladorProductos = new Models.Producto();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorProductos = Datos;
                ControladorProductos.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorProductos.Fecha_Registro = "GETDATE()";
                if (ControladorProductos.Captura.Equals("I"))
                {
                    ControladorProductos.Producto_ID = int.Parse(ControladorProductos.ConsultMaximo().Rows[0]["Producto_ID"].ToString()).ToString("00000");
                    
                        if (ControladorProductos.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorProductos.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultarproducto/{id}")]
        [HttpGet]
  

        public string consultarproducto(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorProductos.Producto_ID = usuario;
            Dt_Data = ControladorProductos.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        [Route("api/listaproductos")]
        [HttpGet]
        public string listaproductos()
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2ListaProductos> list = new List<Select2ListaProductos>();
            try
            {
                ControladorProductos.Estatus = "ACTIVO";
                Dt_Registros = ControladorProductos.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2ListaProductos
                    {
                        id = row.Field<String>("Producto_ID").ToString(),
                        text = row.Field<String>("Nombre_Producto").ToString().Trim(),
                        preciou = row.Field<Decimal>("Precio_Unitario").ToString().Trim(),
                        precioi = row.Field<Decimal>("Costo").ToString().Trim(),
                        tipoinsumo = row.Field<String>("Tipo_Producto").ToString().Trim(),
                        unidad = row.Field<String>("Unidad").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }


    }
}

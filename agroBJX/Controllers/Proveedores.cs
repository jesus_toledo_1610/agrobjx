﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class ProveedoresController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Proveedor ControladorProveedores;

        public ProveedoresController()
        {
            ControladorProveedores = new Proveedor();
        }

        [Route("api/proveedores")]
        [HttpPost]
        public string proveedores( Models.Proveedor Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Proveedor ControladorProveedores = new Models.Proveedor();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorProveedores = Datos;
                ControladorProveedores.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorProveedores.Fecha_Registro = "GETDATE()";
                if (ControladorProveedores.Captura.Equals("I"))
                {
                    ControladorProveedores.Proveedor_ID = int.Parse(ControladorProveedores.ConsultMaximoProveedor().Rows[0]["Proveedor_ID"].ToString()).ToString("00000");
                    
                        if (ControladorProveedores.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorProveedores.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultarproveedor/{id}")]
        [HttpGet]
  

        public string consultarcliente(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorProveedores.Proveedor_ID = usuario;
            Dt_Data = ControladorProveedores.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        [Route("api/listaproveedores")]
        [HttpGet]
        public string listaproveedores()
        {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorProveedores.Estatus = "ACTIVO";
                Dt_Registros = ControladorProveedores.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<String>("Proveedor_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }

    }
}

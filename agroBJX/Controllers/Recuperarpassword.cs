﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;
using System.IO;
using EnviarCorreo;

namespace agroBJX.Controllers
{
    public class RecuperarpasswordController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Usuario ControladorUsuario;

        public RecuperarpasswordController()
        {
            ControladorUsuario = new Usuario();
        }

        [Route("api/recupera")]
        [HttpPost]
        public string recupera( Models.Usuario Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();            
            Models.Usuario ControladorUsuario = new Models.Usuario();
            String StrDatos = String.Empty;
            DataTable dt = new DataTable();
            String respuesta = "No hay respuesta del sevidor";
            string email = ""; string pass = "";string cor = "";
            try
            {
                cor = Datos.Email;
                //ControladorUsuario = Datos;
                ControladorUsuario.Estatus = "ACTIVO";
                dt=ControladorUsuario.Consult();
                if (dt.Rows.Count > 0) {
                    foreach (DataRow row in dt.Rows) {
                        email = Seguridad.DesEncriptar(row["Email"].ToString());
                        
                        if (email == cor) {
                            pass = Seguridad.DesEncriptar(row["Password"].ToString());
                            crearcorreo(pass, email);
                            respuesta = "Registro exitoso.";
                        }
                        
                    }
                    
                }
              
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }
        public void crearcorreo(string pass, string corr)
        {

            string content = String.Empty;
            string dir = System.AppDomain.CurrentDomain.BaseDirectory + "Temporal\\";
            content = File.ReadAllText(HttpContext.Current.Server.MapPath("../Content/Recuperar.html"));
            DataTable parametros = new DataTable();
            parametros = ControladorUsuario.Consult_Parametros();
            enviaremail(pass, corr, content, parametros);

        }

        public static void enviaremail(string pas, string email,  string contents, DataTable parametros)
        {
            SendEmail Obj_Email = new SendEmail();
            string user = ""; string usuario = ""; string pass = "";
            pass = pas;
            usuario = email;
            contents = contents.Replace("[pass]", pass);
            contents = contents.Replace("[user]", usuario);
            Obj_Email.Subject = "Recuperacion de contraseña";
            Obj_Email.Texto = contents;
            Obj_Email.Recibe = usuario;
            Obj_Email.Enviar_Correo_Recuperar(Obj_Email, parametros);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EnviarCorreo
{
    public class SendEmail
    {
        //datos del mail
        public String Recibe { get; set; }
        public String Subject { get; set; }
        public String Texto { get; set; }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Enviar_Correo
        ///DESCRIPCIÓN: Realiza el envio de correos
        ///CREO:      Daniel Alejandro Gonzalez Balladares
        ///FECHA_CREO: 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Enviar_Correo(SendEmail Datos, DataTable Parametros)
        {
            String[] correo = Datos.Recibe.Split(';');
            
                MailMessage email = new MailMessage();
                email.To.Clear();

                for (int i = 0; i < correo.Length; i++)
                    email.To.Add(correo[i]);

                email.From = new MailAddress(Parametros.Rows[0]["Correo"].ToString(), "Registro en Sistema");
                email.Subject = Datos.Subject;
                email.SubjectEncoding = System.Text.Encoding.UTF8;

                email.Body = Datos.Texto;
                email.BodyEncoding = System.Text.Encoding.UTF8;
                email.IsBodyHtml = true;
                //Envia el correo
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Parametros.Rows[0]["Proveedor"].ToString();
                smtp.Port = (int)Parametros.Rows[0]["Puerto"];
                smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential(Parametros.Rows[0]["Correo"].ToString(), Parametros.Rows[0]["Contrasena"].ToString());
                
                    smtp.EnableSsl = true;

                smtp.Send(email);
                email = null;

            
        }

        public void Enviar_Correo_Recuperar(SendEmail Datos, DataTable Parametros)
        {
            String[] correo = Datos.Recibe.Split(';');

            MailMessage email = new MailMessage();
            email.To.Clear();

            for (int i = 0; i < correo.Length; i++)
                email.To.Add(correo[i]);

            email.From = new MailAddress(Parametros.Rows[0]["Correo"].ToString(), "Recuperacion de datos");
            email.Subject = Datos.Subject;
            email.SubjectEncoding = System.Text.Encoding.UTF8;

            email.Body = Datos.Texto;
            email.BodyEncoding = System.Text.Encoding.UTF8;
            email.IsBodyHtml = true;
            //Envia el correo
            SmtpClient smtp = new SmtpClient();
            smtp.Host = Parametros.Rows[0]["Proveedor"].ToString();
            smtp.Port = (int)Parametros.Rows[0]["Puerto"];
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential(Parametros.Rows[0]["Correo"].ToString(), Parametros.Rows[0]["Contrasena"].ToString());

            smtp.EnableSsl = true;

            smtp.Send(email);
            email = null;


        }


    }
}

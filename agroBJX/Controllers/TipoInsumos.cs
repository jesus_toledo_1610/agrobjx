﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class TipoInsumosController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private TipoInsumo ControladorTipoInsumo;

        public TipoInsumosController()
        {
            ControladorTipoInsumo = new TipoInsumo();
        }

        [Route("api/tipoinsumos")]
        [HttpPost]
        public string tipoinsumos( Models.TipoInsumo Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.TipoInsumo ControladorTipoInsumo = new Models.TipoInsumo();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorTipoInsumo = Datos;
                ControladorTipoInsumo.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorTipoInsumo.Fecha_Registro = "GETDATE()";
                if (ControladorTipoInsumo.Captura.Equals("I"))
                {
                    ControladorTipoInsumo.Tipo_Insumo_ID = int.Parse(ControladorTipoInsumo.ConsultMaximoTipoInsumo().Rows[0]["Tipo_Insumo_ID"].ToString()).ToString("00000");
                    
                        if (ControladorTipoInsumo.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorTipoInsumo.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultartipoinsumo/{id}")]
        [HttpGet]
  

        public string consultartipoinsumo(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorTipoInsumo.Tipo_Insumo_ID = usuario;
            Dt_Data = ControladorTipoInsumo.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        [Route("api/listatipoinsumos")]
        [HttpGet]
        public string listatipoinsumos() {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorTipoInsumo.Estatus = "ACTIVO";
                Dt_Registros = ControladorTipoInsumo.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<String>("Tipo_Insumo_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }
    }
}

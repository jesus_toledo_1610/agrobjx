﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using agroBJX.Models.Deal;
using System.Web.Script.Serialization;
using System.Data;
using agroBJX.Models.Assistant;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace agroBJX.Controllers
{
    public class UnidadesController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Unidad ControladorUnidades;

        public UnidadesController()
        {
            ControladorUnidades = new Unidad();
        }

        [Route("api/unidades")]
        [HttpPost]
        public string unidades( Models.Unidad Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            
            
            Models.Unidad ControladorTipoInsumo = new Models.Unidad();
            String StrDatos = String.Empty;
            String respuesta = "No hay respuesta del sevidor";
            try
            {
                ControladorUnidades = Datos;
                ControladorUnidades.Usuario_Registro = Cls_Sessiones.Nombre_Usuario;
                ControladorUnidades.Fecha_Registro = "GETDATE()";
                if (ControladorUnidades.Captura.Equals("I"))
                {
                    ControladorUnidades.Unidad_ID = int.Parse(ControladorUnidades.ConsultMaximo().Rows[0]["Unidad_ID"].ToString()).ToString("00000");
                    
                        if (ControladorUnidades.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                            
                            respuesta = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorUnidades.MasterManagement( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        
                        respuesta = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta = ex.Message;
            }
            return respuesta;
        }

        [Route("api/consultarunidad/{id}")]
        [HttpGet]
  

        public string consultarunidad(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorUnidades.Unidad_ID = usuario;
            Dt_Data = ControladorUnidades.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
          
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        [Route("api/listaunidades")]
        [HttpGet]
        public string listaunidades() {
            string q = "";
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorUnidades.Estatus = "ACTIVO";
                Dt_Registros = ControladorUnidades.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<String>("Unidad_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return JsonConvert.SerializeObject(list, Formatting.None);
        }
    }
}

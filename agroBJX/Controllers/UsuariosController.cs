﻿using agroBJX.Datos.Datos;
using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using EnviarCorreo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace agroBJX.Controllers
{
    public class UsuariosController : ApiController
    {
        agroBJXEntities db = new agroBJXEntities();
        private Usuario ControladorUsuario;
        private Accesos controladorAccesos;
        [Route("api/Salir")]
        [HttpPost]
        public void Salir()
        {
            Cls_Sessiones.Nombre_Usuario = null;
            Cls_Sessiones.No_Usuario = null;
            FormsAuthentication.SignOut();
        }
        public UsuariosController()
        {
            ControladorUsuario = new Usuario();
            controladorAccesos = new Accesos();
        }

        [Route("api/usuarios")]
        [HttpPost]
        public string usuarios( Models.Usuario Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            List<Mdl_Accesos> Obj_Acceso = new List<Mdl_Accesos>();
            
            Models.Usuario ControladorUsuario = new Models.Usuario();
            String StrDatos = String.Empty;
            try
            {
                ControladorUsuario =Datos;
                ControladorUsuario.Usuario_Registro = SessionSRG.Nombre;
                ControladorUsuario.Fecha_Registro = "GETDATE()";
                if (ControladorUsuario.Captura.Equals("I"))
                {
                    ControladorUsuario.Usuario_ID = int.Parse(ControladorUsuario.ConsultMaximoUsuario().Rows[0]["Usuario_ID"].ToString()).ToString("00000");
                    
                        if (ControladorUsuario.MasterManagement1(Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                        {
                         HttpContext.Current.Session["usuario"]= ControladorUsuario.Usuario_ID;
                        HttpContext.Current.Session["captura"] = ControladorUsuario.Captura;
                        crearcorreo(ControladorUsuario.Password,ControladorUsuario.Email,ControladorUsuario.Nombre);
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                        }
                    

                }
                else
                {
                    if (ControladorUsuario.MasterManagement1( Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        HttpContext.Current.Session["usuario"] = ControladorUsuario.Usuario_ID;
                        HttpContext.Current.Session["captura"] = ControladorUsuario.Captura;
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }


        [Route("api/accesos")]
        [HttpPost]
        public string accesos(Models.Accesos[] Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            List<Accesos> Obj_Acceso = new List<Accesos>();
            Models.Accesos controladoraccesos = new Models.Accesos();
            Models.Usuario ControladorUsuario = new Models.Usuario();
            String StrDatos = String.Empty;
            try
            {
                Obj_Acceso = Datos.ToList();
                    controladoraccesos.Usuario_ID = (string)HttpContext.Current.Session["usuario"];
                controladoraccesos.Captura = (string)HttpContext.Current.Session["captura"];


                if (controladoraccesos.Captura.Equals("I"))
                {


                    if (controladoraccesos.MasterManagement(Obj_Acceso, Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }


                }
                else
                {
                    if (controladoraccesos.MasterManagement(Obj_Acceso,Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }


        [Route("api/consultar/{id}")]
        [HttpGet]
  

        public string consultar(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            ControladorUsuario.Usuario_ID = usuario;
            Dt_Data = ControladorUsuario.Consult();
            
            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
            foreach(DataRow row in Dt_Data.Rows)
            {
                string email = Seguridad.DesEncriptar(row["Email"].ToString());
                string pass = Seguridad.DesEncriptar(row["Password"].ToString());
                row["Email"] = email;
                row["Password"] = pass;
            }
            return JsonConvert.SerializeObject(Dt_Data,Formatting.None);
        }

        //funcion para cargar los accesos de los empleados
        [Route("api/consultaaccesos/{id}")]
        [HttpGet]
        public string GetAccessList(int id)
        {
            Respuesta2 ObjMensajeServidor = new Respuesta2();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Data = new DataTable();
            String StrDatos = String.Empty;
            string usuario = String.Format("{0:00000}", id);
            controladorAccesos.Usuario_ID = usuario;
            Dt_Data = controladorAccesos.Consult_Accesos_Usuarios();

            ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Data, Formatting.None);
            StrDatos = Serializer.Serialize(ObjMensajeServidor);
            
            return JsonConvert.SerializeObject(Dt_Data, Formatting.None);
        }


        public void crearcorreo(string pass,string corr, string user)
        {

            string content = String.Empty;
            string dir = System.AppDomain.CurrentDomain.BaseDirectory + "Temporal\\";
            content = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/Confirmacion.html"));
            DataTable parametros = new DataTable();
            parametros = ControladorUsuario.Consult_Parametros();
            enviaremail(pass,corr,user, content,parametros);

        }

        public static void enviaremail(string pas, string email, string usuari,string contents,DataTable parametros)
        {
            SendEmail Obj_Email = new SendEmail();
            string user = "";string usuario = ""; string pass = "";
            pass = pas;
            usuario = email;
            user = usuari;
            contents = contents.Replace("[pass]", pass);
            contents = contents.Replace("[user]", usuario);
            contents = contents.Replace("[usuario]", user);
            Obj_Email.Subject = "Registro en el sistema";
            Obj_Email.Texto = contents;
            Obj_Email.Recibe = usuario;
            Obj_Email.Enviar_Correo(Obj_Email, parametros);
        }
        [Route("api/ejemplo")]
        [HttpPost]
        public string prueba()
        {
            return "ejemplo exitoso";
        }
    }
}

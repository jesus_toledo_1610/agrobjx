﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace agroBJX.Core
{
    /// <summary>
    /// Estructura que representa un filtro para una consulta a la BD.
    /// 
    /// Autor: Francisco Javier Becerra Toledo
    /// Fecha: 03/08/2016
    /// </summary>
    public struct DetallesBD
    {
        /// <summary>
        /// Nombre del campo en la BD.
        /// </summary>
        public List<TablaDB> Tabla;
        /// <summary>
        /// Valor que tendra el campo.
        /// </summary>
        public MODO_DE_CAPTURA Captura;
        /// <summary>
        /// Inicializa una nueva instancia del parámetro con los valores proporcionados.
        /// </summary>       
        public DetallesBD(List<TablaDB> Tabla = null, MODO_DE_CAPTURA Captura = MODO_DE_CAPTURA.CAPTURA_ALTA)
        {
            this.Tabla = Tabla;
            this.Captura = Captura;
        }
    }
    /// <summary>
    /// Estructura auxiliar para manejar campos de clases en BD.
    /// </summary>
    public struct ParametroBD
    {
        //Variables privadas que contendran los valores de las propiedades
        private String campoBD;
        private String valorBD;
        /// <summary>
        /// Nombre del campo en la BD.
        /// </summary>
        public String CampoBD
        {
            get { return campoBD; }
            set { campoBD = value; }
        }
        /// <summary>
        /// Valor que tendra el campo.
        /// </summary>
        public String ValorBD
        {
            get { return valorBD; }
            set { valorBD = value; }
        }
        /// <summary>
        /// Inicializa una nueva instancia del parámetro con los valores proporcionados.
        /// </summary>
        /// <param name="campoBD">Nombre del campo en la BD.</param>
        /// <param name="aliasCampoBD">Alias del campo.</param>
        /// <param name="valorBD">Valor del campo en la BD.</param>
        public ParametroBD(String campoBD = "", String valorBD = "")
        {
            this.campoBD = campoBD;
            this.valorBD = valorBD;
        }
    }

    /// <summary>
    /// Estructura que representa un filtro para una consulta a la BD.
    /// 
    /// Autor: Francisco Javier Becerra Toledo
    /// Fecha: 03/08/2016
    /// </summary>
    public struct FiltroBD
    {
        /// <summary>
        /// Nombre del campo en la BD.
        /// </summary>
        public String CampoBD;
        /// <summary>
        /// Valor que tendra el campo.
        /// </summary>
        public Object ValorBD;       
        /// <summary>
        /// Inicializa una nueva instancia del parámetro con los valores proporcionados.
        /// </summary>
        public FiltroBD(String campoBD = "", Object valorBD = null)
        {
            this.CampoBD = campoBD;
            this.ValorBD = valorBD;
        }
    }

    /// <summary>
    /// Enumeración que indicara el modo de captura en los formularios, 
    /// CAPTURA_NUEVO Indicara una nueva insercción en la BD
    /// CAPTURA_ACTUALIZACION Indicara una actualización a algún registro.
    /// CAPTURA_ELIMA Indicara Eliminar a algún registro.
    /// 
    /// Autor: Francisco Javier Becerra Toledp
    /// Fecha: 01/08/2016
    /// </summary>
    public enum MODO_DE_CAPTURA
    {
        CAPTURA_ALTA,
        CAPTURA_ACTUALIZA,
        CAPTURA_ELIMINAR,
        CAPTURA_ESTATUS,
        CAPTURA_ALTA_IDENTITY
    }
}
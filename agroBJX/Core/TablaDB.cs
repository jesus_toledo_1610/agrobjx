﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace agroBJX.Core
{
    public interface TablaDB
    {
        /// Nombre de la tabla en la BD.
        /// </summary>
        String Tabla { get; }
        /// <summary>
        /// Nombre del id de la tabla.
        /// </summary>
        String ID { get; }
        /// <summary>
        /// Método para obtener los parámetros para insertar en la BD.
        /// </summary>
        /// <returns>Lista de parámetros para interactuar con la BD.</returns>
        List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura);
    }
}
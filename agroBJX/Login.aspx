﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="agroBJX.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>ERP BJX</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Portal TD Solutions">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <link rel="shortcut icon" href="favicontd.ico">
    <!-- jQuery -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <!-- FontAwesome JS-->
    <script defer src="assets/plugins/fontawesome/js/all.min.js"></script>
    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="assets/css/portal.css">

    <script src="assets/js/acceso/login.js"></script>
</head>
<body class="app app-login p-0">
    <div class="row g-0 app-auth-wrapper">
        <div class="col-12 col-md-12 col-lg-12 auth-main-col text-center p-5">
            <div class="d-flex flex-column align-content-end">
                <div class="app-auth-body mx-auto">
                    <div class="app-auth-branding mb-4"><a class="app-logo">
                        <img class="logo-icon mr-2" src="assets/images/logo.jpeg" alt="logo"></a></div>
                    <h2 class="auth-heading text-center mb-5">Sistema Administrativo y Comercial</h2>
                    <div class="auth-form-container text-left">
                        <form id="frmLogin" class="auth-form login-form">
                            <div class="email mb-3">
                                <label class="sr-only" for="signin-email">Email</label>
                                <input id="signin-email" name="signin-email" type="email" class="form-control signin-email" placeholder="Correo Electronico" required="required">
                            </div>
                            <!--//form-group-->
                            <div class="password mb-3">
                                <label class="sr-only" for="signin-password">Password</label>
                                <input id="signin-password" name="signin-password" type="password" class="form-control signin-password" placeholder="Contrase&ntilde;a" required="required">
                                <div id="div_error_acceso" class="alert alert-danger" role="alert">
                                    <p id="msjLogin"></p>
                                </div>

                                <div class="extra mt-3 row justify-content-between">
                                    <div class="col-3">
                                    </div>
                                    <div class="col-6">
                                        <div class="forgot-password text-center">
                                            <a href="resetpassword.aspx">Olvide mi contrase&ntilde;a</a>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                    </div>
                                    <!--//col-6-->
                                </div>
                                <!--//extra-->
                            </div>
                            <!--//form-group-->
                            <div class="text-center">
                                <button type="submit" class="btn app-btn-primary btn-block theme-btn mx-auto">Iniciar Sessi&oacute;n</button><center>
								<div class="fa-3x">
									<i class="fas fa-cog fa-spin"></i>
								</div>
                            </div>
                        </form>
                    </div>
                    <!--//auth-form-container-->

                </div>
                <!--//auth-body-->
               

                <footer class="app-auth-footer">
                    <div class="container text-center py-3">

                        <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
                        <small class="copyright">Copyright © 2021 Powered by  <a href="http://www.td-solutions.com.mx/" target="_blank">TD Solutions</a> MX - Todos los derechos reservados.</small>

                    </div>
                </footer>
                <!--//app-auth-footer-->
            </div>
            <!--//flex-column-->
        </div>
        <!--//auth-main-col-->
        <!-- espacio de color para mostrar frente a la imagen-->

         <div class="col-12 col-md-5 col-lg-6 h-100 auth-background-col" style="display: none">
                    <div class="auth-background-holder">
                    </div>
                    <div class="auth-background-mask"></div>
                    <div class="auth-background-overlay p-3 p-lg-5" style="display: none">
                        <div class="d-flex flex-column align-content-end h-100">
                            <div class="h-100"></div>
                            <%-- <div class="overlay-content p-3 p-lg-4 rounded">
					    <h5 class="mb-3 overlay-title">TD Solutions Base Bootstrap</h5>
					    <div>Plantilla base de Bootstrap 5 admin dashboard sin Backend</div>
				    </div>--%>
                        </div>
                    </div>
                    <!--//auth-background-overlay-->
                </div>
                <!--//auth-background-col-->
    </div>
    <!--//row-->

</body>
</html>

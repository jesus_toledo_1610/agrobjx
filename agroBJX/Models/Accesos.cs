﻿using agroBJX.Core;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class Accesos : TablaDB
    {
        public String Usuario_ID { get; set; }
        public String MenuID { get; set; }
        public String ParentID { get; set; }
        public String Nombre { get; set; }
        public String Controlador { get; set; }
        public String Accion { get; set; }
        public String Captura { get; set; }



        public Accesos(String Usuario_ID = "", String MenuID = "", String ParentID = "", String Nombre = "",
            String Controlador = "", String Accion = "",string Captura="")
        {
            this.Usuario_ID = Usuario_ID;
            this.MenuID = MenuID;
            this.ParentID = ParentID;
            this.Nombre = Nombre;
            this.Controlador = Controlador;
            this.Accion = Accion;

        }

        public DataTable Consult() { return Ctrl_Accesos.Consultaccess(this); }
        public DataTable Consultoperaciones() { return Ctrl_Accesos.Consultoperaciones(this); }
        public DataTable Consultsuboperaciones() { return Ctrl_Accesos.Consultsuboperaciones(this); }
        public DataTable Consult2() { return Ctrl_Accesos.Consultaccess2(this); }

        public DataTable Consult_Accesos_Usuarios() { return Ctrl_Accesos.Consultaccess_Usuarios(this); }

        public Boolean MasterManagement(List<Accesos> Accesos, MODO_DE_CAPTURA captura) { return Ctrl_Usuarios.MasterManagement(this, captura, Accesos); }
        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Apl_Accesos"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Usuario_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("Usuario_ID", Usuario_ID.Trim()));
            parametrosBD.Add(new ParametroBD("Menu_ID", MenuID));
            parametrosBD.Add(new ParametroBD("Nombre", Nombre));
            if (!String.IsNullOrEmpty(ParentID))
                parametrosBD.Add(new ParametroBD("Parent_ID", ParentID));
            if (!String.IsNullOrEmpty(Controlador))
                parametrosBD.Add(new ParametroBD("Controlador", Controlador));
            if (!String.IsNullOrEmpty(Accion))
                parametrosBD.Add(new ParametroBD("Accion", Accion));
            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
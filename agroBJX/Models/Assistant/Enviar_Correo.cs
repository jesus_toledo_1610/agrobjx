﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.VisualBasic;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using agroBJX.Models.Data;
using agroBJX.Models.Deal;

namespace agroBJX.Models.Assistant
{
    public class Enviar_Correo
    {
        #region (VARIABLES)
        //datos del mail
        private String Envia;
        private String Recibe;
        private String Subject;
        private String Texto;
        //datos de configuracion
        private String Servidor;
        private String Password;
        private String Puerto;
        private String SSL;
        private String Adjunto;        
        #endregion

        #region (VARIABLES PUBLICAS)
        public String P_Adjunto
        {
            get { return Adjunto; }
            set { Adjunto = value; }
        }
        public string P_Envia
        {
            get { return Envia; }
            set { Envia = value; }
        }
        public string P_Recibe
        {
            get { return Recibe; }
            set { Recibe = value; }
        }
        public string P_Subject
        {
            get { return Subject; }
            set { Subject = value; }
        }
        public string P_Texto
        {
            get { return Texto; }
            set { Texto = value; }
        }
        public string P_Servidor
        {
            get { return Servidor; }
            set { Servidor = value; }
        }
        public string P_Password
        {
            get { return Password; }
            set { Password = value; }
        }

        public string P_Puerto
        {
            get { return Puerto; }
            set { Puerto = value; }
        }        
        #endregion

        #region (METODOS)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Enviar_Correo_Generico
        ///DESCRIPCIÓN: Realiza el envio de correos
        ///PROPIEDADES: 
        ///METODOS    : 
        ///CREO:        José Antonio López Hernández
        ///FECHA_CREO: 11/Septiembre/2015 13:40
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
         #endregion
    }
}
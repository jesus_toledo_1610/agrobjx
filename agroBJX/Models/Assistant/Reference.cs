﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace agroBJX.Models.Assistant
{
    public class Select2Model
    {
        public String id { get; set; }
        public String text { get; set; }
        public String Data2 { get; set; }
        public String Data3 { get; set; }
        public String Data4 { get; set; }
        public String Data5 { get; set; }
        public String Data6 { get; set; }
        public String Data7 { get; set; }
    }

    public class Select2ListaFleteras
    {
        public string id { get; set; }
        public String text { get; set; }
        public String contacto { get; set; }
        public string telefono { get; set; }
    }

    public class Select2ListaAduana
    {
        public string id { get; set; }
        public String text { get; set; }
        public String contacto { get; set; }
        public string telefono { get; set; }
    }

    public class Select2ListaProductos
    {
        public string id { get; set; }
        public String text { get; set; }
        public String preciou { get; set; }
        public string tipoinsumo { get; set; }
        public string precioi { get; set; }
        public string unidad { get; set; }
    }
    public class Select2ListaDirecciones
    {
        public string id { get; set; }
        public String text { get; set; }
        public String colonia { get; set; }
        public string cp { get; set; }
        public string ciudad { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
    }
    public class Select2ListaCliente
    {
        public string id { get; set; }
        public String text { get; set; }
        public String razon { get; set; }
        public string rfc { get; set; }
        public string ubicacion { get; set; }
        public string contacto { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
    }
    public class Select2ListaEmpleados
    {
        public String id { get; set; }
        public String text { get; set; }
        public String Apellido_P { get; set; }
        public String Apellido_M { get; set; }
        public String Nombre { get; set; }
        public String No_Empleado { get; set; }
        public String Telefono { get; set; }
        public String Empresa { get; set; }
        public String Correo { get; set; }
        public String UnidadNegocio { get; set; }
    }

    public class Respuesta
    {
        public String Mensaje { get; set; }
        public Boolean Estatus { get; set; }
        public String Data { get; set; }
        public String Data2 { get; set; }
        public String Data3 { get; set; }
        public String Data4 { get; set; }
        public String Ruta { get; set; }
        public string Url_PDF { get; set; }
    }

    public class Respuesta2
    {
        public String Data { get; set; }
    }


    public class Login
    {
        [Required(ErrorMessage = "Se requiere un usuario")]
        //[RegularExpression(".+\\@.+\\..+",
        //ErrorMessage = "Se requiere un usuario valido")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Se requiere una Contraseña")]
        public String Password { get; set; }
    }

    public static class Seguridad
    {
        /// Encripta una cadena
        public static String Encriptar(this String _cadenaAencriptar)
        {
            String result = String.Empty;
            Byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        /// Esta función desencripta la cadena que le envíamos en el parámentro de entrada.
        public static String DesEncriptar(this String _cadenaAdesencriptar)
        {
            String result = String.Empty;
            Byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }
    }

    public class SessionSRG
    {
        public static String Nombre
        {
            set { HttpContext.Current.Session["Nombre"] = value; }
            get
            {
                if (HttpContext.Current.Session["Nombre"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["Nombre"].ToString();
            }
        }

        public static String No_Usuario
        {
            set { HttpContext.Current.Session["No_Usuario"] = value; }
            get
            {
                if (HttpContext.Current.Session["No_Usuario"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["No_Usuario"].ToString();
            }
        }
    }
}
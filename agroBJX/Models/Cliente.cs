﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class Cliente : TablaDB
    {
        public String Cliente_ID { get; set; }
        public String Nombre { get; set; }
        public String Razon_Social { get; set; }
        public String RFC { get; set; }
        public String Estatus { get; set; }
        public String Direccion { get; set; }
        public String Ciudad { get; set; }
        public String Colonia { get; set; }
        public String C_P { get; set; }
        public String Estado { get; set; }
        public String Telefono { get; set; }
        public String Email { get; set; }
        public String Limite_Credito { get; set; }
        public String Dias_Credito { get; set; }
        public String Numero_E { get; set; }
        public String Numero_I { get; set; }
        public String Pais { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public string Captura { get; set; }
        public Cliente(String Nombre = "", String Estatus = "", String Email = "",string Razon_Social="",string Direccion="", string Ciudad="", string Colonia="", string C_P="",
            string Estado="", string Telefono="", string Limite_Credito="", string Dias_Credito="", string Numero_E="", string Numero_I="", string Pais="",
            string RFC="", String Usuario_Registro = "", String Fecha_Registro = "")
        {
            this.Cliente_ID = Cliente_ID;
            this.Nombre = Nombre;
            this.Estatus = Estatus;
            this.Email = Email;
            this.Razon_Social = Razon_Social;
            this.Direccion = Direccion;
            this.Ciudad = Ciudad;
            this.Colonia = Colonia;
            this.C_P = C_P;
            this.RFC = RFC;
            this.Estado = Estado;
            this.Telefono = Telefono;
            this.Limite_Credito = Limite_Credito;
            this.Dias_Credito = Dias_Credito;
            this.Numero_E = Numero_E;
            this.Numero_I = Numero_I;
            this.Pais = Pais;            
            this.Usuario_Registro = Usuario_Registro;

        }
       
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Clientes.MasterManagement(this, captura); }
       
        public DataTable Consult() { return Ctrl_Clientes.ConsultCliente(this); }
        public DataTable ConsultMaximoCliente() { return Ctrl_Clientes.ConsultMaximoCliente(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Clientes"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Cliente_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Cliente_ID", Cliente_ID.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Razon_Social))
                parametrosBD.Add(new ParametroBD("Razon_Social", Razon_Social.Trim()));
            if (!String.IsNullOrEmpty(RFC))
                parametrosBD.Add(new ParametroBD("RFC", RFC.Trim()));

            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Direccion))
                parametrosBD.Add(new ParametroBD("Direccion", Direccion.Trim()));
            if (!String.IsNullOrEmpty(Email))
                parametrosBD.Add(new ParametroBD("Email",Email.Trim()));
            if (!String.IsNullOrEmpty(Ciudad))
                parametrosBD.Add(new ParametroBD("Ciudad", Ciudad.Trim()));
            if (!String.IsNullOrEmpty(Colonia))
                parametrosBD.Add(new ParametroBD("Colonia", Colonia.Trim()));
            if (!String.IsNullOrEmpty(C_P))
                parametrosBD.Add(new ParametroBD("C_P", C_P.Trim()));
            if (!String.IsNullOrEmpty(Estado))
                parametrosBD.Add(new ParametroBD("Estado", Estado.Trim()));
            if (!String.IsNullOrEmpty(Telefono))
                parametrosBD.Add(new ParametroBD("Telefono", Telefono.Trim()));
            if (!String.IsNullOrEmpty(Dias_Credito))
                parametrosBD.Add(new ParametroBD("Dias_Credito", Dias_Credito.Trim()));
            if (!String.IsNullOrEmpty(Limite_Credito))
                parametrosBD.Add(new ParametroBD("Limite_Credito", Limite_Credito.Trim()));
            if (!String.IsNullOrEmpty(Numero_E))
                parametrosBD.Add(new ParametroBD("Numero_E", Numero_E.Trim()));
            else
                parametrosBD.Add(new ParametroBD("Numero_E", ""));
            if (!String.IsNullOrEmpty(Numero_I))
                parametrosBD.Add(new ParametroBD("Numero_I", Numero_I.Trim()));
            if (!String.IsNullOrEmpty(Pais))
                parametrosBD.Add(new ParametroBD("Pais", Pais.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
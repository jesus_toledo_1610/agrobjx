﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using agroBJX.Models.Deal;
using agroBJX.Models.Assistant;
using SRG_APLExamenes.Models.Assistant;

namespace agroBJX.Models.Data
{
    public class Ctrl_Accesos
    {
      //funcion para cargar solo nombres de los accesos para lel menu
        internal static DataTable Consultaccess(Accesos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Apl_Accesos WHERE Usuario_ID = @Usuario_ID and Parent_ID=1 Order By Menu_ID", con))
                {
                    cmd.Parameters.AddWithValue("@Usuario_ID", Dato.Usuario_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
                con.Close();
            }
            return Dt_Registro;
        }

        //funcion para cargar menu operaciones
        internal static DataTable Consultoperaciones(Accesos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Apl_Accesos WHERE Usuario_ID = @Usuario_ID and Parent_ID=2 Order By Menu_ID", con))
                {
                    cmd.Parameters.AddWithValue("@Usuario_ID", Dato.Usuario_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
                con.Close();
            }
            return Dt_Registro;
        }

        //funcion para cargar solo accesos del menu paretn id
        internal static DataTable Consultaccess2(Accesos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Apl_Accesos WHERE Usuario_ID = @Usuario_ID and Parent_ID >2 and Parent_ID<13 Order By Menu_ID", con))
                {
                    cmd.Parameters.AddWithValue("@Usuario_ID", Dato.Usuario_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
                con.Close();
            }
            return Dt_Registro;
        }

        //funcion para cargar submenu de operaciones
        internal static DataTable Consultsuboperaciones(Accesos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Apl_Accesos WHERE Usuario_ID = @Usuario_ID and Parent_ID >=13 Order By Menu_ID", con))
                {
                    cmd.Parameters.AddWithValue("@Usuario_ID", Dato.Usuario_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
                con.Close();
            }
            return Dt_Registro;
        }

        //funcion para cargar todos los accesos de los usuarios
        internal static DataTable Consultaccess_Usuarios(Accesos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Apl_Accesos WHERE Usuario_ID = @Usuario_ID and Parent_ID !=1 Order By Menu_ID", con))
                {
                    cmd.Parameters.AddWithValue("@Usuario_ID", Dato.Usuario_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
                con.Close();
            }
            return Dt_Registro;
        }
    }
}
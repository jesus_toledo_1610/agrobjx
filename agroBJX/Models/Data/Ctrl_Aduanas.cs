﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Data
{
    public class Ctrl_Aduanas
    {
        
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
//funcion para consultar las Aduanas del sistema
        internal static DataTable ConsultAduana(Aduana Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
          
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Estatus = @Status";
                else
                    Query += " WHERE Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.Aduana_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Aduana_ID = @NoA";
                else
                    Query += " WHERE Aduana_ID = @NoA";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select * from Cat_Aduanas" + Query + " order by Aduana_ID asc", con))
                {
                   
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Aduana_ID))
                        cmd.Parameters.AddWithValue("@NoA", Dato.Aduana_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }



    //funcion para sacar el maximo de Aduana
        internal static DataTable ConsultMaximoAduana(Aduana Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Aduana_ID), 0) + 1 AS Aduana_ID FROM Cat_Aduanas", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}
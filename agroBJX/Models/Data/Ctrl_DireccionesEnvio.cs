﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Data
{
    public class Ctrl_DireccionesEnvio
    {
        
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
//funcion para consultar las Aduanas del sistema
        internal static DataTable Consult(DireccionEnvio Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
          
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND d.Estatus = @Status";
                else
                    Query += " WHERE d.Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.Direccion_Envio_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND d.Direccion_Envio_ID = @NoA";
                else
                    Query += " WHERE d.Direccion_Envio_ID = @NoA";
            }
            if (!String.IsNullOrEmpty(Dato.Cliente_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND c.Cliente_ID = @NoC";
                else
                    Query += " WHERE c.Cliente_ID = @NoC";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select d.*,d.Ciudad + ' '+ d.Estado as Ubicacion, c.Nombre as Cliente from Cat_Direcciones_Envio d " +
                    " inner join Cat_Clientes c on d.Cliente_ID=c.Cliente_ID " + Query + " order by d.Direccion_Envio_ID asc", con))
                {
                   
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Direccion_Envio_ID))
                        cmd.Parameters.AddWithValue("@NoA", Dato.Direccion_Envio_ID);

                    if (!String.IsNullOrEmpty(Dato.Cliente_ID))
                        cmd.Parameters.AddWithValue("@NoC", Dato.Cliente_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }



    //funcion para sacar el maximo de Aduana
        internal static DataTable ConsultMaximo(DireccionEnvio Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Direccion_Envio_ID), 0) + 1 AS Direccion_Envio_ID FROM Cat_Direcciones_Envio", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}
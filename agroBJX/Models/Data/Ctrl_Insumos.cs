﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Data
{
    public class Ctrl_Insumos
    {
        
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
//funcion para consultar los tipos de insumo del sistema
        internal static DataTable Consult(Insumo Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
          
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Estatus = @Status";
                else
                    Query += " WHERE Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.Insumo_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND i.Insumo_ID = @NoI";
                else
                    Query += " WHERE i.Insumo_ID = @NoI";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select i.*, u.Nombre as Unidad,u.Unidad_ID, p.Nombre as Proveedor,p.Proveedor_ID, " +
                    "t.Tipo_Insumo_ID, t.Nombre as TipoInsumo from Cat_Insumos i inner join Cat_Unidades u on i.Unidad_ID = u.Unidad_ID " +
                    " inner join Cat_Proveedores p on i.Proveedor_ID = p.Proveedor_ID  inner join Cat_Tipo_Insumo t on i.Tipo_Insumo_ID = t.Tipo_Insumo_ID" + Query + " order by i.Insumo_ID asc", con))
                {
                   
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Insumo_ID))
                        cmd.Parameters.AddWithValue("@NoI", Dato.Insumo_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }



        //funcion para sacar el maximo de Tipo_Insumo_ID
        internal static DataTable ConsultMaximo(Insumo Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Insumo_ID), 0) + 1 AS Insumo_ID FROM Cat_Insumos", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}
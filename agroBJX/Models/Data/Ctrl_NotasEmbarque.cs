﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Data
{
    public class Ctrl_NotasEmbarque
    {
        
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
//funcion para consultar las notas del sistema
        internal static DataTable Consult(NotaEmbarque Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
          
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND e.Estatus = @Status";
                else
                    Query += " WHERE e.Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.Embarque_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND e.Embarque_ID = @NoA";
                else
                    Query += " WHERE e.Embarque_ID = @NoA";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select e.*,c.Nombre as Cliente, c.Cliente_ID, f.Nombre as Fletera, f.Fletera_ID, a.Nombre as Aduana, a.Aduana_ID," +
                    " p.Nombre as Producto, p.Producto_ID,p.Clave_Producto, p.Clave_Producto + '- '+p.Nombre as Nombre_Producto, de.Direccion_Envio, de.Direccion_Envio_ID from Ope_Notas_Embarque e " +
                    " inner join Cat_Clientes c on c.Cliente_ID=e.Cliente_ID " +
                    " inner join Cat_Fleteras f on f.Fletera_ID=e.Fletera_ID " +
                    " inner join  Cat_Aduanas a on a.Aduana_ID = e.Aduana_ID " +
                    "inner join Cat_Productos p on p.Producto_ID = e.Producto_ID " +
                    " inner join Cat_Direcciones_Envio de on de.Direccion_Envio_ID = e.Direccion_Envio_ID " + Query + " order by e.Embarque_ID asc", con))
                {
                   
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Embarque_ID))
                        cmd.Parameters.AddWithValue("@NoA", Dato.Embarque_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }



    //funcion para sacar el maximo de Aduana
        internal static DataTable ConsultMaximo(NotaEmbarque Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Embarque_ID), 0) + 1 AS Embarque_ID FROM Ope_Notas_Embarque", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}
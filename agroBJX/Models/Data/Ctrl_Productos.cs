﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Data
{
    public class Ctrl_Productos
    {
        
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
//funcion para consultar los tipos de insumo del sistema
        internal static DataTable Consult(Producto Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
          
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND p.Estatus = @Status";
                else
                    Query += " WHERE p.Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.Producto_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND p.Producto_ID = @Nop";
                else
                    Query += " WHERE p.Producto_ID = @Nop";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select p.*,p.Clave_Producto +' - ' + p.Nombre as Nombre_Producto,  u.Nombre as Unidad,u.Unidad_ID from Cat_Productos" +
                    " p inner join Cat_Unidades u on p.Unidad_ID = u.Unidad_ID " +
                      Query + " order by p.Producto_ID asc", con))
                {
                   
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Producto_ID))
                        cmd.Parameters.AddWithValue("@Nop", Dato.Producto_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }



        //funcion para sacar el maximo de Producto_ID
        internal static DataTable ConsultMaximo(Producto Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Producto_ID), 0) + 1 AS Producto_ID FROM Cat_Productos", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}
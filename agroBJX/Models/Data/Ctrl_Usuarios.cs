﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Data
{
    public class Ctrl_Usuarios
    {
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura, List<Accesos> Accesos)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Database.BD);
            SqlCommand Obj_Comando = new SqlCommand();
            SqlTransaction Obj_Transaccion = null;
            Boolean Transaccion = false;
            String Temporal = String.Empty;
            Accesos Obj_Temp = new Accesos();
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";

                Temporal = QuerySQL.ObtenValorDeParametro(Elemento.ObtenParametros(Captura), "Usuario_ID").ToString();
                for (int i = 0; i < Accesos.Count; i++)
                {
                    Accesos[i].Usuario_ID = Temporal;
                }
                List<TablaDB> Lista_Accesos = Accesos.ToList<TablaDB>();
                Accesos = new List<Accesos>();
                Obj_Temp.Usuario_ID = Temporal;
                Accesos.Add(Obj_Temp);
                List<TablaDB> Lista_Accesos_Temp = Accesos.ToList<TablaDB>();
                Query += QuerySQL.GeneraCMDExecDetalles(
                    new DetallesBD(Lista_Accesos_Temp, MODO_DE_CAPTURA.CAPTURA_ELIMINAR),
                    new DetallesBD(Lista_Accesos, MODO_DE_CAPTURA.CAPTURA_ALTA));

                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                Obj_Comando.CommandText = Query;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                Obj_Transaccion.Rollback();
                throw new Exception(Ex.Message);
            }
            finally
            {
                if (!Transaccion)
                {
                    Obj_Conexion.Close();
                }
            }
            return Transaccion;
        }
        internal static Boolean MasterManagement1(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        
        //funcion para consultar los usuarios en el sistema
        internal static DataTable ConsultUsuario(Usuario Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (!String.IsNullOrEmpty(Dato.Password))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND CONVERT(VARCHAR (MAX), Password) ='"+Dato.Password+"'";
                else
                    Query += " WHERE CONVERT(VARCHAR (MAX), Password) ='"+Dato.Password+"'";
            }
            if (!String.IsNullOrEmpty(Dato.Email))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND CONVERT(VARCHAR (MAX), Email) ='"+Dato.Email+"'";
                else
                    Query += " WHERE CONVERT(VARCHAR (MAX), Email) ='"+Dato.Email+"'";
            }
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Estatus = @Status";
                else
                    Query += " WHERE Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.Usuario_ID))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Usuario_ID = @NoEmp";
                else
                    Query += " WHERE Usuario_ID = @NoEmp";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select * from Cat_Usuarios" + Query + " order by Usuario_ID asc", con))
                {
                    if (!String.IsNullOrEmpty(Dato.Password))
                        cmd.Parameters.AddWithValue("@employeePassword", Seguridad.Encriptar(Dato.Password.Trim()));
                    if (!String.IsNullOrEmpty(Dato.Email))
                        cmd.Parameters.AddWithValue("@mail", Seguridad.Encriptar(Dato.Email.Trim()));
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Usuario_ID))
                        cmd.Parameters.AddWithValue("@NoEmp", Dato.Usuario_ID);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        //
        internal static DataTable ConsultParametros(Usuario Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
          
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();


                using (SqlCommand cmd = new SqlCommand("select * from Parametros", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        //funcion para consultar el maximo usuario id
        internal static DataTable ConsultMaximoUsuario(Usuario Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Usuario_ID), 0) + 1 AS Usuario_ID FROM Cat_Usuarios", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}
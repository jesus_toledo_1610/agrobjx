﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using agroBJX.Core;
using agroBJX.Models.Data;

namespace agroBJX.Models.Deal
{
    public class Mdl_Accesos : TablaDB
    {
        public String Usuario_ID { get; set; }
        public String MenuID { get; set; }
        public String ParentID { get; set; }
        public String Nombre { get; set; }
        public String Controlador { get; set; }
        public String Accion { get; set; }

        public Mdl_Accesos() { }

        public Mdl_Accesos(String Usuario_ID = "", String MenuID = "", String ParentID = "", String Nombre = "",
            String Controlador = "", String Accion = "")
        {
            this.Usuario_ID = Usuario_ID;
            this.MenuID = MenuID;
            this.ParentID = ParentID;
            this.Nombre = Nombre;
            this.Controlador = Controlador;
            this.Accion = Accion;
        }

        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Apl_Accesos"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Usuario_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("Usuario_ID", Usuario_ID.Trim()));
            parametrosBD.Add(new ParametroBD("Menu_ID", MenuID));
            parametrosBD.Add(new ParametroBD("Nombre", Nombre));
            if(!String.IsNullOrEmpty(ParentID))
                parametrosBD.Add(new ParametroBD("Parent_ID", ParentID));
            if (!String.IsNullOrEmpty(Controlador))
                parametrosBD.Add(new ParametroBD("Controlador", Controlador));
            if (!String.IsNullOrEmpty(Accion))
                parametrosBD.Add(new ParametroBD("Accion", Accion));
            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models.Deal
{
    public class Mdl_Usuarios : TablaDB
    {
        public String Usuario_ID { get; set; }
        public String Nombre { get; set; }
        public String Apellido_Paterno { get; set; }
        public String Apellido_Materno { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Estatus { get; set; }
        public String Captura { get; set; }


        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Usuarios"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Usuario_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Usuario_ID", Usuario_ID.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Apellido_Materno))
                parametrosBD.Add(new ParametroBD("Apellido_Materno", Apellido_Materno.Trim()));
            if (!String.IsNullOrEmpty(Apellido_Paterno))
                parametrosBD.Add(new ParametroBD("Apellido_Paterno", Apellido_Paterno.Trim()));

            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Password))
                parametrosBD.Add(new ParametroBD("Password", Seguridad.Encriptar(Password.Trim())));
            if (!String.IsNullOrEmpty(Email))
                parametrosBD.Add(new ParametroBD("Email", Seguridad.Encriptar(Email.Trim())));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
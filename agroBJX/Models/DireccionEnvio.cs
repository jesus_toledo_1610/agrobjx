﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class DireccionEnvio : TablaDB
    {
        public String Direccion_Envio_ID { get; set; }
        public String Cliente_ID { get; set; }
        public String Estatus { get; set; }
        public String Direccion_Envio { get; set; }
        public String Calle { get; set; }
        public String Colonia { get; set; }
        public String CP { get; set; }
        public String Estado { get; set; }
        public String Pais { get; set; }
        public String Ciudad { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public string Captura { get; set; }
        public DireccionEnvio(String Cliente_ID = "", String Estatus = "", String Direccion_Envio = "",string Calle="", string Colonia="",string CP="", string Estado="", string Pais="", string Ciudad="", String Usuario_Registro = "", String Fecha_Registro = "")
        {
            this.Direccion_Envio_ID = Direccion_Envio_ID;
            this.Cliente_ID = Cliente_ID;
            this.Estatus = Estatus;
            this.Direccion_Envio = Direccion_Envio;
            this.Calle = Calle;
            this.Colonia = Colonia;
            this.CP = CP;
            this.Estado = Estado;
            this.Pais = Pais;
            this.Ciudad = Ciudad;
            this.Usuario_Registro = Usuario_Registro;

        }
       
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_DireccionesEnvio.MasterManagement(this, captura); }
       
        public DataTable Consult() { return Ctrl_DireccionesEnvio.Consult(this); }
        public DataTable ConsultMaximo() { return Ctrl_DireccionesEnvio.ConsultMaximo(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>                      Cat_Direcciones_Envio
        public String Tabla { get { return "Cat_Direcciones_Envio"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Direccion_Envio_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Direccion_Envio_ID", Direccion_Envio_ID.Trim()));
            if (!String.IsNullOrEmpty(Cliente_ID))
                parametrosBD.Add(new ParametroBD("Cliente_ID", Cliente_ID.Trim()));
            if (!String.IsNullOrEmpty(Direccion_Envio))
                parametrosBD.Add(new ParametroBD("Direccion_Envio", Direccion_Envio.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Calle))
                parametrosBD.Add(new ParametroBD("Calle", Calle.Trim()));
            if (!String.IsNullOrEmpty(Colonia))
                parametrosBD.Add(new ParametroBD("Colonia", Colonia.Trim()));
            if (!String.IsNullOrEmpty(CP))
                parametrosBD.Add(new ParametroBD("CP", CP.Trim()));
            if (!String.IsNullOrEmpty(Estado))
                parametrosBD.Add(new ParametroBD("Estado", Estado.Trim()));
            if (!String.IsNullOrEmpty(Pais))
                parametrosBD.Add(new ParametroBD("Pais", Pais.Trim()));
            if (!String.IsNullOrEmpty(Ciudad))
                parametrosBD.Add(new ParametroBD("Ciudad", Ciudad.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class Fletera : TablaDB
    {
        public String Fletera_ID { get; set; }
        public String Nombre { get; set; }
        public String Estatus { get; set; }
        public String Contacto { get; set; }
        public String Telefono { get; set; }
        public String Email { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public string Captura { get; set; }
        public Fletera(String Nombre = "", String Estatus = "", String Email = "",string Contacto="", string Telefono="", String Usuario_Registro = "", String Fecha_Registro = "")
        {
            this.Fletera_ID = Fletera_ID;
            this.Nombre = Nombre;
            this.Estatus = Estatus;
            this.Email = Email;
            this.Contacto = Contacto;
            this.Telefono = Telefono;         
            this.Usuario_Registro = Usuario_Registro;

        }
       
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Fleteras.MasterManagement(this, captura); }
       
        public DataTable Consult() { return Ctrl_Fleteras.ConsultFletera(this); }
        public DataTable ConsultMaximoFletera() { return Ctrl_Fleteras.ConsultMaximoFletera(this); }
    
        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Fleteras"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Fletera_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Fletera_ID", Fletera_ID.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Contacto))
                parametrosBD.Add(new ParametroBD("Contacto", Contacto.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Email))
                parametrosBD.Add(new ParametroBD("Email",Email.Trim()));
            if (!String.IsNullOrEmpty(Telefono))
                parametrosBD.Add(new ParametroBD("Telefono", Telefono.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
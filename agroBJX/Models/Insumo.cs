﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class Insumo : TablaDB
    {
        public String Insumo_ID { get; set; }
        public String Nombre { get; set; }
        public String Tipo_Insumo_ID { get; set; }
        public String Unidad_ID { get; set; }
        public String Proveedor_ID { get; set; }
        public String Costo { get; set; }
        public String IVA { get; set; }
        public String IEPS { get; set; }
        public String Total { get; set; }
        public String Existencias { get; set; }
        public String Estatus { get; set; }
        public String Descripcion { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public string Captura { get; set; }
        public Insumo(String Nombre = "", String Estatus = "", String Tipo_Insumo_ID = "" ,string Unidad_ID="", string Proveedor_ID="", string Costo="",  string IVA="", string IEPS="", string Total="", string Existencias="",String Usuario_Registro = "", String Fecha_Registro = "")
        {
            this.Insumo_ID = Insumo_ID;
            this.Nombre = Nombre;
            this.Existencias = Existencias;
            this.Total = Total;
            this.IVA = IVA;
            this.IEPS = IEPS;
            this.Costo = Costo;
            this.Tipo_Insumo_ID = Tipo_Insumo_ID;
            this.Proveedor_ID = Proveedor_ID;
            this.Unidad_ID = Unidad_ID;
            this.Estatus = Estatus;
            this.Descripcion = Descripcion;       
            this.Usuario_Registro = Usuario_Registro;

        }
       
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Insumos.MasterManagement(this, captura); }
       
        public DataTable Consult() { return Ctrl_Insumos.Consult(this); }
        public DataTable ConsultMaximo() { return Ctrl_Insumos.ConsultMaximo(this); }
    
        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Insumos"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Insumo_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Insumo_ID", Insumo_ID.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Tipo_Insumo_ID))
                parametrosBD.Add(new ParametroBD("Tipo_Insumo_ID", Tipo_Insumo_ID.Trim()));
            if (!String.IsNullOrEmpty(Unidad_ID))
                parametrosBD.Add(new ParametroBD("Unidad_ID", Unidad_ID.Trim()));
            if (!String.IsNullOrEmpty(Proveedor_ID))
                parametrosBD.Add(new ParametroBD("Proveedor_ID", Proveedor_ID.Trim()));
            if (!String.IsNullOrEmpty(Costo))
                parametrosBD.Add(new ParametroBD("Costo", Costo.Trim()));
            if (!String.IsNullOrEmpty(IVA))
                parametrosBD.Add(new ParametroBD("IVA", IVA.Trim()));
            if (!String.IsNullOrEmpty(IEPS))
                parametrosBD.Add(new ParametroBD("IEPS", IEPS.Trim()));
            if (!String.IsNullOrEmpty(Total))
                parametrosBD.Add(new ParametroBD("Total", Total.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
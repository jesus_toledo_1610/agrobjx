﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class NotaEmbarque : TablaDB
    {
        public String Embarque_ID { get; set; }
        public String Fecha_Embarque { get; set; }
        public String Cliente_ID { get; set; }
        public String Direccion_Envio_ID { get; set; }
        public String Fletera_ID { get; set; }
        public String Aduana_ID { get; set; }
        public String Numero_Cajas { get; set; }
        public String Producto_ID { get; set; }
        public String Precio_Unitario { get; set; }
        public String Precio_Aduana { get; set; }
        public String Precio_Fletera { get; set; }
        public String SubTotal { get; set; }
        public String Total { get; set; }
        public String Estatus { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public string Captura { get; set; }
        public NotaEmbarque(string Fecha_Embarque="", string Cliente_ID="", string Direccion_Envio_ID="", string Fletera_ID="",
            string Aduana_ID="", string Numero_Cajas="", string Producto_ID="", string Precio_Unitario="", string Precio_Aduana="",
            string Precio_Fletera="", string Subtotal="", string Total="", string Estatus="", String Usuario_Registro = "", String Fecha_Registro = "")
        {
            this.Embarque_ID = Embarque_ID;
            this.Fecha_Embarque = Fecha_Embarque;
            this.Cliente_ID = Cliente_ID;
            this.Direccion_Envio_ID = Direccion_Envio_ID;
            this.Fletera_ID = Fletera_ID;
            this.Aduana_ID = Aduana_ID;
            this.Numero_Cajas = Numero_Cajas;
            this.Producto_ID = Producto_ID;
            this.Precio_Unitario = Precio_Unitario;
            this.Precio_Aduana = Precio_Aduana;
            this.Precio_Fletera = Precio_Fletera;
            this.SubTotal = Subtotal;
            this.Total = Total;
            this.Estatus = Estatus;
            this.Usuario_Registro = Usuario_Registro;

        }
       
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_NotasEmbarque.MasterManagement(this, captura); }
       
        public DataTable Consult() { return Ctrl_NotasEmbarque.Consult(this); }
        public DataTable ConsultMaximo() { return Ctrl_NotasEmbarque.ConsultMaximo(this); }
    
        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Notas_Embarque"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Embarque_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Embarque_ID", Embarque_ID.Trim()));
            if (!String.IsNullOrEmpty(Fecha_Embarque))
                parametrosBD.Add(new ParametroBD("Fecha_Embarque", Fecha_Embarque.Trim()));
            if (!String.IsNullOrEmpty(Cliente_ID))
                parametrosBD.Add(new ParametroBD("Cliente_ID", Cliente_ID.Trim()));
            if (!String.IsNullOrEmpty(Direccion_Envio_ID))
                parametrosBD.Add(new ParametroBD("Direccion_Envio_ID", Direccion_Envio_ID.Trim()));
            if (!String.IsNullOrEmpty(Fletera_ID))
                parametrosBD.Add(new ParametroBD("Fletera_ID", Fletera_ID.Trim()));
            if (!String.IsNullOrEmpty(Aduana_ID))
                parametrosBD.Add(new ParametroBD("Aduana_ID", Aduana_ID.Trim()));

            if (!String.IsNullOrEmpty(Numero_Cajas))
                parametrosBD.Add(new ParametroBD("Numero_Cajas", Numero_Cajas.Trim()));
            if (!String.IsNullOrEmpty(Producto_ID))
                parametrosBD.Add(new ParametroBD("Producto_ID", Producto_ID.Trim()));
            if (!String.IsNullOrEmpty(Precio_Unitario))
                parametrosBD.Add(new ParametroBD("Precio_Unitario", Precio_Unitario.Trim()));
            if (!String.IsNullOrEmpty(Precio_Aduana))
                parametrosBD.Add(new ParametroBD("Precio_Aduana", Precio_Aduana.Trim()));

            if (!String.IsNullOrEmpty(Precio_Fletera))
                parametrosBD.Add(new ParametroBD("Precio_Fletera", Precio_Fletera.Trim()));
            if (!String.IsNullOrEmpty(SubTotal))
                parametrosBD.Add(new ParametroBD("Sub_Total", SubTotal.Trim()));
            if (!String.IsNullOrEmpty(Total))
                parametrosBD.Add(new ParametroBD("Total", Total.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
﻿using agroBJX.Core;
using agroBJX.Models.Assistant;
using agroBJX.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace agroBJX.Models
{
    public class Producto : TablaDB
    {
        public String Producto_ID { get; set; }
        public String Nombre { get; set; }
        public String Clave_Producto { get; set; }
        public String Tipo_Producto { get; set; }
        public String Unidad_ID { get; set; }
        public String Precio_Unitario { get; set; }
        public String IVA { get; set; }
        public String IEPS { get; set; }
        public String Costo { get; set; }
        public String Estatus { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public string Captura { get; set; }
        public Producto(String Nombre = "", String Estatus = "", String Clave_Producto = "" ,string Tipo_Producto="", string Unidad_ID="", string Precio_Unitario="",
            string IVA="", string IEPS="", string Costo="",String Usuario_Registro = "", String Fecha_Registro = "")
        {
            this.Producto_ID = Producto_ID;
            this.Nombre = Nombre;
            this.Estatus = Estatus;
            this.Tipo_Producto = Tipo_Producto;
            this.Precio_Unitario = Precio_Unitario;
            this.Unidad_ID = Unidad_ID;
            this.Clave_Producto = Clave_Producto;
            this.Tipo_Producto = Tipo_Producto;
            this.IVA = IVA;
            this.IEPS = IEPS;
            this.Costo = Costo;
            this.Usuario_Registro = Usuario_Registro;

        }
       
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Productos.MasterManagement(this, captura); }
       
        public DataTable Consult() { return Ctrl_Productos.Consult(this); }
        public DataTable ConsultMaximo() { return Ctrl_Productos.ConsultMaximo(this); }
    
        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Productos"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Producto_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Producto_ID", Producto_ID.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Tipo_Producto))
                parametrosBD.Add(new ParametroBD("Tipo_Producto", Tipo_Producto.Trim()));
            if (!String.IsNullOrEmpty(Clave_Producto))
                parametrosBD.Add(new ParametroBD("Clave_Producto", Clave_Producto.Trim()));
            if (!String.IsNullOrEmpty(Unidad_ID))
                parametrosBD.Add(new ParametroBD("Unidad_ID", Unidad_ID.Trim()));
            if (!String.IsNullOrEmpty(IVA))
                parametrosBD.Add(new ParametroBD("IVA", IVA.Trim()));
            if (!String.IsNullOrEmpty(IEPS))
                parametrosBD.Add(new ParametroBD("IEPS", IEPS.Trim()));
            if (!String.IsNullOrEmpty(Costo))
                parametrosBD.Add(new ParametroBD("Costo", Costo.Trim()));
            if (!String.IsNullOrEmpty(Precio_Unitario))
                parametrosBD.Add(new ParametroBD("Precio_Unitario", Precio_Unitario.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {

            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}
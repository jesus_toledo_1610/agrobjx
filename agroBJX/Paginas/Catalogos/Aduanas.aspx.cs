﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class Aduanas :Page
    {
        public Aduana ControladorClientes = new Aduana();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario)) {
                GetAduanas();
            }
            else {
                Response.Redirect("../../Login.aspx");
            }
        }

        

        
        public IEnumerable<string[]> GetAduanas()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorClientes.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Aduana_ID"] is DBNull ? string.Empty : Fila.Field<String>("Aduana_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila.Field<String>("Contacto") == null ? string.Empty :  Fila.Field<String>("Contacto").Trim()),
                                     (Fila.Field<String>("Email") == null ? string.Empty : Fila.Field<String>("Email").Trim()),
                                     (Fila.Field<String>("Telefono") == null ? string.Empty : Fila.Field<String>("Telefono").Trim()),
                                     
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
﻿<%@ Page Title="Clientes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="agroBJX.Paginas.Catalogos.Clientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../assets/js/acceso/Clientes.js?v=1.0.1"></script>
    <script>

            function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
        }

    
                function solonumeros(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = "0123456789",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
        }
    </script>
    <link id="theme-style" rel="stylesheet" href="../../Content/bootstrap-theme.css">
    
<div id="Pn-Empleado" class="col-lg-12">

    <div class="row">
        <div class="col-lg-5"></div>
           <div class="col-lg-2">
        
      <label  class="form-label" style="color:white; text-align:center; font-size:25px;" ><strong>Clientes</strong></label>
            
    </div>
        <div class="col-lg-5"></div>
    </div>
 
    <section class="container-fluid br-nav">
        <nav class="navbar navbar-expand-lg navbar-dark rounded myClass">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                    <button id="btn-new" type="button" class="btn btn-primary" > <strong style="color:white">Nuevo</strong> <i style="color:white" class="fa fa-plus-circle"></i></button>
                </li>
            </ul>
          
            <%--<ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <button id="btn-cancel" type="button" class="btn btn-danger" style='display: none; margin-right:60px;'>Cancelar <i class="fa fa-times-circle bigicon2" aria-hidden="true"></i></button>
                </li>
                <li class="nav-item">
                    <button id="btn-save" type="button" class="btn btn-success" style='display: none; margin-right:60px;'>Guardar <i class="fa fa-save bigicon2"></i></button>
                </li>
            </ul>--%>
        </nav>
    </section>

<div class="container" id="wrapper" style="display:none;">
    <div class="row">
        <div class="col-lg-12">
            <div class="well well-sm">
                  <div class="col-12 col-lg-12">
		                <div class="app-card app-card-settings shadow-sm p-4">
						    
						    <div class="app-card-body">
							    <form class="frm-register"  onsubmit="return false" id="datosusuario">
                                      <div class="row">
                                          <div class="col-lg-8"></div>
                                        <div class="col-lg-2">
                                            <button id="btn-save-cliente" type="button" class="btn btn-success btn-lg" > <strong style="color:white">Guardar</strong> <i class="fa fa-save"></i></button>
                                        </div>
                                        
                                        <div class="col-lg-2">
                                            <button id="btn-cancel" type="button" class="btn btn-danger" style='display: none; margin-right:60px;'><strong style="color:white">Regresar</strong> <i class="fa fa-times-circle bigicon2" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                         <div class="col-lg-2" style="display:none">
									    <label  class="form-label">Cliente ID</label>
									    <input type="text" class="form-control" id="txt-ClienteID" placeholder="Cliente ID" disabled="disabled">
									</div>

                                      <div class="col-lg-6">
									    <label class="form-label">Razon Social</label>
									    <input type="text" class="form-control" id="txt-razon" placeholder="Razon Social" required onkeypress="return soloLetras(event)" maxlength="100">
									</div>

                                    <div class="col-lg-3">
									    <label  class="form-label">RFC</label>
									    <input type="text" class="form-control" id="txt-rfc" placeholder="RFC" required maxlength="100">
									</div>
                                        
                                     <div class="col-lg-3">
									     <label class="form-label">Estatus</label>
                                          <br />
                                      <select id="cmb-stt" class="form-control">
                                         <option value="">Seleccione </option>
                                         <option value="ACTIVO">ACTIVO</option>
                                         <option value="INACTIVO">INACTIVO</option>
                                    </select>
									</div>

                                    </div>
                                
                                    <hr />
                                    <br />
                                    <div class="row">

                                    <div class="col-lg-6">
									    <label  class="form-label">Calle</label>
                                          <input id="txt-dir" name="direccion" type="text" class="form-control" placeholder="Calle" required="required" maxlength="150">
							       </div>

                                     <div class="col-lg-3">
									    <label  class="form-label">Numero Exterior</label>
                                          <input id="txt-ne" name="NE" type="text" class="form-control" placeholder="Numero Exterior" required="required" onkeypress="return solonumeros(event)" maxlength="20">
							       </div>

                                    <div class="col-lg-3">
									    <label  class="form-label">Numero Interior</label>
                                          <input id="txt-ni" name="NI" type="text" class="form-control" placeholder="Numero Interior" required="required" maxlength="20" >
							       </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                         <div class="col-lg-5">
									    <label  class="form-label">Colonia</label>
                                          <input id="txt-col" name="Colonia" type="text" class="form-control" placeholder="Colonia" required="required" maxlength="100">
									</div>
                                      <div class="col-lg-3">
									    <label  class="form-label">C.P</label>
                                          <input id="txt-cp" name="CP" type="text" class="form-control" placeholder="C.P" required="required" maxlength="5" onkeypress="return solonumeros(event)">
									</div>

                                     <div class="col-lg-3">
									    <label  class="form-label">Ciudad</label>
                                          <input id="txt-ciudad" name="ciudad" type="text" class="form-control" placeholder="Ciudad" required="required" onkeypress="return soloLetras(event)" maxlength="100">
									</div>

                                    </div> 
                                    <br />
                                    <div class="row">

                                    <div class="col-lg-3">
									    <label  class="form-label">Estado</label>
                                          <input id="txt-estado" name="estado" type="text" class="form-control" placeholder="Estado" required="required" onkeypress="return soloLetras(event)" maxlength="50">
									</div>

                                     <div class="col-lg-3">
									    <label  class="form-label">Pais</label>
                                          <input id="txt-pais" name="pais" type="text" class="form-control" placeholder="Pais" required="required" onkeypress="return soloLetras(event)" maxlength="50">
									</div>

                                    </div>

                                   <hr />
								   <br />
                                 <div class="row">
                                      <div class="col-lg-4">
									    <label  class="form-label">Contacto</label>
									    <input type="text" class="form-control" id="txt-nombre" placeholder="Contacto" onkeypress="return soloLetras(event)" required maxlength="50">
									</div>

                                    <div class="col-lg-4">
									    <label  class="form-label">Telefono</label>
                                          <input id="txt-tel" name="Telefono" type="text" class="form-control" placeholder="Telefono" required onkeypress="return solonumeros(event)" maxlength="10" >
									</div>

                                    <div class="col-lg-4">
									    <label  class="form-label">Correo</label>
                                          <input id="txt-email" name="signin-email" type="email" class="form-control signin-email" placeholder="Correo Electronico" required="required" maxlength="150">
									    <%--<input type="email" class="form-control" id="txt-email" placeholder="Correo" required>--%>
									</div>

                                   </div>
                                    <br />
                                <div class="row">

                                     <div class="col-lg-4">
									    <label  class="form-label">Dias Credito</label>
									    <input type="text" class="form-control" id="txt-dc" placeholder="Dias Credito" required onkeypress="return solonumeros(event)" maxlength="5">
									</div>

                                    <div class="col-lg-4">
									    <label  class="form-label">Limite Credito</label>
									    <input type="text" class="form-control" id="txt-lc" placeholder="Limite Credito" required onkeypress="return solonumeros(event)" maxlength="20">
									</div>

                                </div>
							    </form>
						    </div><!--//app-card-body-->
						    
						</div><!--//app-card-->
	                </div>
            </div>
        </div>
    </div>

</div><div class="modal" id="Ventana_Espera">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Procesando</h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
    	<div class="tab-content" id="tablausuarios" >
			        <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
					    <div class="app-card app-card-orders-table shadow-sm mb-5">
						    <div class="app-card-body">
							    <div class="table-responsive">
							        <table class="table app-table-hover mb-0 text-left">
										<thead>
											<tr>
												<th class="cell" style="text-align:left">No Cliente</th>
												<th class="cell" style="text-align:left">Razon Social</th>
												<th class="cell">RFC</th>
												<th class="cell">Ubicacion</th>
												<th class="cell">Estatus</th>
												<th class="cell">Editar</th>
											</tr>
										</thead>
										<tbody>
										 <asp:ListView ID="Clienteslist" runat="server" ItemType="System.String[]" 
                                SelectMethod="Getclientes" ViewStateMode="Disabled">
                                    <ItemTemplate>
                                        <tr  id="Producto1_<%#: Item[0] %>">
                                           <%-- <td class="ti-close1 " id="<%#: Item[0] %>">  <asp:Button CssClass="botoncito"  runat="server" ID="btnImprime"  OnClick="btnimprime_Click" value="<%#: Item[0] %>" /> </td>
                                           --%> <td class="cart-title first-row" style="text-align:left">
                                               <input class="producto" id="Producto" value="<%#: Item[0] %>" hidden="hidden" ></input> <span> <%#: Item[0] %></span>
                                            </td>
                                            <td style="text-align:left" >
                                                <span><%#: Item[1] %></span>
                                            </td>
                                              <td style="text-align:left">
                                                <span><%#: Item[2] %></span>
                                            </td>
                                              <td style="text-align:left" >
                                                <span><%#: Item[3] %></span>
                                            </td>
                                            <td style="text-align:left" >
                                                <span><%#: Item[4] %></span>
                                            </td>
                                            <td>
                                            <%--<input class="fas fa-edit" id="Edit" type="button" value="<%#: Item[0] %>"></input> --%>
                                             <button  type="button" class="btn btn-success btn-lg tr"  value="<%#: Item[0] %>" id="<%#: Item[0] %>" > <strong style="color:white"></strong> <i class="fa fa-edit"></i></button>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <div>
                                            <p>No hay Registros</p>
                                        </div>
                                    </EmptyDataTemplate>
                                </asp:ListView>
		
										</tbody>
									</table>
						        </div><!--//table-responsive-->
						       
						    </div><!--//app-card-body-->		
						</div><!--//app-card-->
						<%--<nav class="app-pagination">
							<ul class="pagination justify-content-center">
								<li class="page-item disabled">
									<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
							    </li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item">
								    <a class="page-link" href="#">Next</a>
								</li>
							</ul>
						</nav>--%><!--//app-pagination-->
						
			        </div><!--//tab-pane-->
				</div><!--//tab-content-->
				
</div>
    
    
</asp:Content>



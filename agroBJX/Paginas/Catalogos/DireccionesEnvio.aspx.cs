﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class DireccionesEnvio :Page
    {
        public DireccionEnvio ControladorDirecciones = new DireccionEnvio();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetDireccionesEnvio();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetDireccionesEnvio()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorDirecciones.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Direccion_Envio_ID"] is DBNull ? string.Empty : Fila.Field<String>("Direccion_Envio_ID").ToString()),
                                     (Fila.Field<String>("Cliente") == null ? string.Empty : Fila.Field<String>("Cliente").Trim()),
                                     (Fila.Field<String>("Direccion_Envio") == null ? string.Empty :  Fila.Field<String>("Direccion_Envio").Trim()),
                                     (Fila.Field<String>("Ubicacion") == null ? string.Empty : Fila.Field<String>("Ubicacion").Trim()),
                                     (Fila.Field<String>("Estatus") == null ? string.Empty : Fila.Field<String>("Estatus").Trim()),
                                     
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
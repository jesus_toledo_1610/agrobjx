﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class Fleteras :Page
    {
        public Fletera ControladorFleteras = new Fletera();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetFleteras();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetFleteras()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorFleteras.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Fletera_ID"] is DBNull ? string.Empty : Fila.Field<String>("Fletera_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila.Field<String>("Contacto") == null ? string.Empty :  Fila.Field<String>("Contacto").Trim()),
                                     (Fila.Field<String>("Email") == null ? string.Empty : Fila.Field<String>("Email").Trim()),
                                     (Fila.Field<String>("Telefono") == null ? string.Empty : Fila.Field<String>("Telefono").Trim()),
                                     
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
﻿<%@ Page Title="Notas Embarque" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NotasEmbarque.aspx.cs" Inherits="agroBJX.Paginas.Catalogos.NotasEmbarque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link id="theme_style1" rel="stylesheet" href="../../Content/Select/select2.min.css">
    <link id="theme_style3" rel="stylesheet" href="../../Content/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker.min.css">
    <link id="theme_style2" rel="stylesheet" href="../../Content/bootstrap-datetimepicker-0.0.11/css/bootstrap-timepicker.css">

    <!-- Minified JS library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script defer src="../../Content/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js"></script>
    <script defer src="../../Content/bootstrap-datetimepicker-0.0.11/js/bootstrap-timepicker.min.js"></script>
    <script defer src="../../Scripts/select2.min.js"></script>
    <script src="../../assets/js/acceso/NotasEmbarque.js?v=1.0.1"></script>
    <script>

        function soloLetras(e) {
            var key = e.keyCode || e.which,
                tecla = String.fromCharCode(key).toLowerCase(),
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
                especiales = [8, 37, 39, 46],
                tecla_especial = false;

            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }
        }


        function solonumeros(e) {
            var key = e.keyCode || e.which,
                tecla = String.fromCharCode(key).toLowerCase(),
                letras = "0123456789",
                especiales = [8, 37, 39, 46],
                tecla_especial = false;

            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }
        }
    </script>
    <link id="theme-style" rel="stylesheet" href="../../Content/bootstrap-theme.css">

    <div id="Pn-Empleado" class="col-lg-12">

        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">

                <label class="form-label" style="color: white; text-align: center; font-size: 25px;"><strong>Notas Embarques</strong></label>

            </div>
            <div class="col-lg-4"></div>
        </div>

        <section class="container-fluid br-nav">
            <nav class="navbar navbar-expand-lg navbar-dark rounded myClass">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <button id="btn-new" type="button" class="btn btn-primary"><strong style="color: white">Nuevo</strong> <i style="color: white" class="fa fa-plus-circle"></i></button>
                    </li>
                </ul>

                <%--<ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <button id="btn-cancel" type="button" class="btn btn-danger" style='display: none; margin-right:60px;'>Cancelar <i class="fa fa-times-circle bigicon2" aria-hidden="true"></i></button>
                </li>
                <li class="nav-item">
                    <button id="btn-save" type="button" class="btn btn-success" style='display: none; margin-right:60px;'>Guardar <i class="fa fa-save bigicon2"></i></button>
                </li>
            </ul>--%>
            </nav>
        </section>

        <div class="container" id="wrapper" style="display: none;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="well well-sm">
                        <div class="col-12 col-lg-12">
                            <div class="app-card app-card-settings shadow-sm p-4">

                                <div class="app-card-body">
                                    <form class="frm-register" onsubmit="return false" id="datosusuario">
                                        <div class="row">
                                            <div class="col-lg-8"></div>
                                            <div class="col-lg-2">
                                                <button id="btn-save-nota" type="button" class="btn btn-success btn-lg"><strong style="color: white">Guardar</strong> <i class="fa fa-save"></i></button>
                                            </div>

                                            <div class="col-lg-2">
                                                <button id="btn-cancel" type="button" class="btn btn-danger" style='display: none; margin-right: 60px;'><strong style="color: white">Regresar</strong> <i class="fa fa-times-circle bigicon2" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-2" style="display: none">
                                                <label class="form-label">Embarque ID</label>
                                                <input type="text" class="form-control" id="txt-NotaID" placeholder="Nota ID" disabled="disabled">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Fecha Embarque</label>
                                                <input type="text" id="txt-fecha_embarque" name="Fecha_Embarque" class="form-control" />
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Cliente </label>
                                                <select id="cmb-cliente" class="form-control" style="width: 100%;"></select>
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Direccion Envio</label>
                                                <select id="cmb-direcciones" class="form-control" style="width: 100%;"></select>
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Fletera</label>
                                                <select id="cmb-fletera" class="form-control" style="width: 100%;"></select>
                                            </div>

                                        </div>
                                        <br />
                                        <div id="infocliente" class="row">
                                            <label class="form-label" style="text-align: center">Informacion Cliente</label><br />
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <label class="form-label">Razon Social</label>
                                                    <input type="text" class="form-control" id="txt-razon" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">RFC</label>
                                                    <input type="text" class="form-control" id="txt-rfc" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Ubicacion</label>
                                                    <input type="text" class="form-control" id="txt-ubicacion" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Contacto</label>
                                                    <input type="text" class="form-control" id="txt-contacto" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Telefono</label>
                                                    <input type="text" class="form-control" id="txt-telefono" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Correo</label>
                                                    <input type="text" class="form-control" id="txt-correo" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div id="infodirecciones" class="row">
                                            <br />
                                            <label class="form-label" style="text-align: center">Informacion Direccion Envio</label><br />
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <label class="form-label">Colonia</label>
                                                    <input type="text" class="form-control" id="txt-colonia" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">CP</label>
                                                    <input type="text" class="form-control" id="txt-cp" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Ciudad</label>
                                                    <input type="text" class="form-control" id="txt-ciudad" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Estado</label>
                                                    <input type="text" class="form-control" id="txt-estado" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Pais</label>
                                                    <input type="text" class="form-control" id="txt-pais" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div id="infofletera" class="row">
                                            <br />
                                            <label class="form-label" style="text-align: center">Informacion Fletera</label><br />
                                            <br />
                                            <div class="row">

                                                <div class="col-lg-2">
                                                    <label class="form-label">Contacto</label>
                                                    <input type="text" class="form-control" id="txt-cfletera" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Telefono</label>
                                                    <input type="text" class="form-control" id="txt-tfletera" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label class="form-label">Aduana</label>
                                                <select id="cmb-aduana" class="form-control" style="width: 100%;"></select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Producto</label>
                                                <select id="cmb-producto" class="form-control" style="width: 100%;"></select>
                                            </div>

                                            <div class="col-lg-2" >
                                                <label class="form-label">Unidad</label>
                                                <input type="text" class="form-control" id="txt-punidad" disabled="disabled">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Estatus</label>
                                                <br />
                                                <select id="cmb-stt" class="form-control">
                                                    <option value="">Seleccione </option>
                                                    <option value="ACTIVO">ACTIVO</option>
                                                    <option value="INACTIVO">INACTIVO</option>
                                                </select>
                                            </div>


                                        </div>
                                        <br />
                                        <div id="infoaduana" class="row">
                                            <br />
                                            <label class="form-label" style="text-align: center">Informacion Aduana</label><br />
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <label class="form-label">Contacto</label>
                                                    <input type="text" class="form-control" id="txt-acontacto" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Telefono</label>
                                                    <input type="text" class="form-control" id="txt-atelefono" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div id="infoproducto" class="row">
                                            <br />
                                            <label class="form-label" style="text-align: center">Informacion Producto</label><br />
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <label class="form-label">Tipo Producto</label>
                                                    <input type="text" class="form-control" id="txt-tproducto" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Precio Unitario</label>
                                                    <input type="text" class="form-control" id="txt-ppreciou" disabled="disabled">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label class="form-label">Precio con Impuestos</label>
                                                    <input type="text" class="form-control" id="txt-pimpuestos" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">

                                            <div class="col-lg-3">
                                                <label class="form-label">Numero Cajas</label>
                                                <input id="txt-ncajas" name="ncajas" type="text" class="form-control" placeholder="Numero Cajas" required="required" value="0">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Precio Unitario</label>
                                                <input id="txt-preciou" name="preciounitario" type="text" class="form-control" required="required" value="0">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Precio Aduana</label>
                                                <input id="txt-precioa" name="precioaduana" type="text" class="form-control" required="required" value="0">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Precio Fletera</label>
                                                <input id="txt-preciof" name="preciofletera" type="text" class="form-control" required="required" value="0">
                                            </div>

                                        </div>
                                        <br />
                                        <div class="row">

                                            <div class="col-lg-3">
                                                <label class="form-label">Subtotal</label>
                                                <input id="txt-sub" name="sub" type="text" class="form-control" placeholder="Subtotal" required="required" disabled="disabled">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Total</label>
                                                <input id="txt-total" name="total" type="text" class="form-control" placeholder="Total" required="required" disabled="disabled">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--//app-card-body-->

                            </div>
                            <!--//app-card-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal" id="Ventana_Espera">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Procesando</h4>
                    </div>
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content" id="tablausuarios">
            <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell">No Embarque</th>
                                        <th class="cell" style="text-align: left">Fecha Embarque</th>
                                        <th class="cell" style="text-align: left">Cliente</th>
                                        <th class="cell">Producto</th>
                                        <th class="cell">Estatus</th>
                                        <th class="cell">Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:ListView ID="Embarqueslist" runat="server" ItemType="System.String[]"
                                        SelectMethod="GetNotasEmbarque" ViewStateMode="Disabled">
                                        <ItemTemplate>
                                            <tr id="Producto1_<%#: Item[0] %>">
                                                <%-- <td class="ti-close1 " id="<%#: Item[0] %>">  <asp:Button CssClass="botoncito"  runat="server" ID="btnImprime"  OnClick="btnimprime_Click" value="<%#: Item[0] %>" /> </td>
                                                --%>
                                                <td class="cart-title first-row" style="text-align: left">
                                                    <input class="producto" id="Producto" value="<%#: Item[0] %>" hidden="hidden"></input>
                                                    <span><%#: Item[0] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[1] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[2] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[3] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[4] %></span>
                                                </td>
                                                <td>
                                                    <%--<input class="fas fa-edit" id="Edit" type="button" value="<%#: Item[0] %>"></input> --%>
                                                    <button type="button" class="btn btn-success btn-lg tr" value="<%#: Item[0] %>" id="<%#: Item[0] %>"><strong style="color: white"></strong><i class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <div>
                                                <p style="color: #5d6778">No hay mas Notas</p>
                                            </div>
                                        </EmptyDataTemplate>
                                    </asp:ListView>

                                </tbody>
                            </table>
                        </div>
                        <!--//table-responsive-->

                    </div>
                    <!--//app-card-body-->
                </div>
                <!--//app-card-->
                <%--<nav class="app-pagination">
							<ul class="pagination justify-content-center">
								<li class="page-item disabled">
									<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
							    </li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item">
								    <a class="page-link" href="#">Next</a>
								</li>
							</ul>
						</nav>--%><!--//app-pagination-->

            </div>
            <!--//tab-pane-->
        </div>
        <!--//tab-content-->

    </div>


</asp:Content>



﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class NotasEmbarque :Page
    {
        public NotaEmbarque ControladorNotas = new NotaEmbarque();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetNotasEmbarque();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetNotasEmbarque()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorNotas.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Embarque_ID"] is DBNull ? string.Empty : Fila.Field<String>("Embarque_ID").ToString()),
                                     (Fila.Field<DateTime>("Fecha_Embarque") == null ? string.Empty : Fila.Field<DateTime>("Fecha_Embarque").ToString("dd/MM/yyyy")),
                                     (Fila.Field<String>("Cliente") == null ? string.Empty :  Fila.Field<String>("Cliente").Trim()),
                                     (Fila.Field<String>("Producto") == null ? string.Empty : Fila.Field<String>("Producto").Trim()),
                                     (Fila.Field<String>("Estatus") == null ? string.Empty : Fila.Field<String>("Estatus").Trim()),
                                     
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
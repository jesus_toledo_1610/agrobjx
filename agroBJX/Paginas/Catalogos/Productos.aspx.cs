﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class Productos :Page
    {
        public Producto ControladorProducto = new Producto();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetProductos();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetProductos()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorProducto.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Producto_ID"] is DBNull ? string.Empty : Fila.Field<String>("Producto_ID").ToString()),
                                     (Fila.Field<String>("Clave_Producto") == null ? string.Empty : Fila.Field<String>("Clave_Producto").Trim()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila.Field<String>("Unidad") == null ? string.Empty :  Fila.Field<String>("Unidad").Trim()),
                                     (Fila.Field<Decimal>("Costo") == 0 ? string.Empty : Fila.Field<Decimal>("Costo").ToString()),
                                     
                                     
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
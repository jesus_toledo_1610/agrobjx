﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class Proveedores :Page
    {
        public Proveedor ControladorProveedores = new Proveedor();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetProveedores();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetProveedores()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorProveedores.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Proveedor_ID"] is DBNull ? string.Empty : Fila.Field<String>("Proveedor_ID").ToString()),
                                     (Fila.Field<String>("Razon_Social") == null ? string.Empty : Fila.Field<String>("Razon_Social").Trim()),
                                     (Fila.Field<String>("RFC") == null ? string.Empty : Fila.Field<String>("RFC").Trim()),
                                     (Fila.Field<String>("Ubicacion") == null ? string.Empty : Fila.Field<String>("Ubicacion").Trim()),
                                     (Fila.Field<String>("Estatus") == null ? string.Empty :  Fila.Field<String>("Estatus").Trim()),
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
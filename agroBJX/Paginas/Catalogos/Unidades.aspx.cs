﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class Unidades :Page
    {
        public Unidad ControladorUnidades = new Unidad();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetUnidades();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetUnidades()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorUnidades.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Unidad_ID"] is DBNull ? string.Empty : Fila.Field<String>("Unidad_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila.Field<String>("Descripcion") == null ? string.Empty :  Fila.Field<String>("Descripcion").Trim()),
                                     (Fila.Field<String>("Estatus") == null ? string.Empty : Fila.Field<String>("Estatus").Trim()),
                                     
                                     
                                     
                                     

                             };
                return result;
            
        }
        

    }
}
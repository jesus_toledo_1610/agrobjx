﻿<%@ Page Title="Usuarios" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Usuarios.aspx.cs" Inherits="agroBJX.Paginas.Catalogos.Usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../assets/js/acceso/Usuarios.js?v=1.0.1"></script>

    <link id="theme-style" rel="stylesheet" href="../../Content/bootstrap-theme.css">

    <div id="Pn-Empleado" class="col-lg-12">

        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-2">

                <label class="form-label" style="color: white; text-align: center; font-size: 25px;"><strong>USUARIOS</strong></label>

            </div>
            <div class="col-lg-5"></div>
        </div>

        <section class="container-fluid br-nav">
            <nav class="navbar navbar-expand-lg navbar-dark rounded myClass">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <button id="btn-new" type="button" class="btn btn-primary"><strong style="color: white">Nuevo</strong> <i style="color: white" class="fa fa-plus-circle"></i></button>
                    </li>
                </ul>

                <%--<ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <button id="btn-cancel" type="button" class="btn btn-danger" style='display: none; margin-right:60px;'>Cancelar <i class="fa fa-times-circle bigicon2" aria-hidden="true"></i></button>
                </li>
                <li class="nav-item">
                    <button id="btn-save" type="button" class="btn btn-success" style='display: none; margin-right:60px;'>Guardar <i class="fa fa-save bigicon2"></i></button>
                </li>
            </ul>--%>
            </nav>
        </section>

        <div class="container" id="wrapper" style="display: none;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="well well-sm">
                        <div class="col-12 col-lg-12">
                            <div class="app-card app-card-settings shadow-sm p-4">

                                <div class="app-card-body">
                                    <form class="frm-register" onsubmit="return false" id="datosusuario">
                                        <div class="row">
                                            <div class="col-lg-8"></div>
                                            <div class="col-lg-2">
                                                <button id="btn-save" type="button" class="btn btn-success btn-lg"><strong style="color: white">Guardar</strong> <i class="fa fa-save"></i></button>
                                            </div>

                                            <div class="col-lg-2">
                                                <button id="btn-cancel" type="button" class="btn btn-danger" style='display: none; margin-right: 60px;'><strong style="color: white">Regresar</strong> <i class="fa fa-times-circle bigicon2" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label class="form-label">Usuario ID</label>
                                                <input type="text" class="form-control" id="txt-usuarioid" placeholder="Usuario ID" disabled="disabled">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Nombre</label>
                                                <input type="text" class="form-control" id="txt-nombre" placeholder="Nombre" required maxlength="150">
                                            </div>
                                            <div class="col-lg-3">
                                                <label class="form-label">Apellido Paterno</label>
                                                <input type="text" class="form-control" id="txt-app" placeholder="Apellido Paterno" required maxlength="150">
                                            </div>

                                            <div class="col-lg-3">
                                                <label class="form-label">Apellido Materno</label>
                                                <input type="text" class="form-control" id="txt-apm" placeholder="Apellido Materno" required maxlength="150">
                                            </div>

                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label class="form-label">Correo</label>
                                                <input id="txt-email" name="signin-email" type="email" class="form-control signin-email" placeholder="Correo Electronico" required="required" maxlength="250">
                                                <%--<input type="email" class="form-control" id="txt-email" placeholder="Correo" required>--%>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="form-label">
                                                    Contraseña<span class="ml-2" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="La Contraseña se enviara al correo registrado"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                        <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z" />
                                                        <circle cx="8" cy="4.5" r="1" />
                                                    </svg></span></label>
                                                <%--<label  class="form-label">Contraseña</label>--%>
                                                <input type="password" class="form-control" id="txt-pass" placeholder="Contraseña" disabled="disabled" required>
                                            </div>

                                            <div class="col-lg-4">
                                                <label class="form-label">Estatus</label>
                                                <br />
                                                <select id="cmb-stt" class="form-control">
                                                    <option value="">Seleccione </option>
                                                    <option value="ACTIVO">ACTIVO</option>
                                                    <option value="INACTIVO">INACTIVO</option>
                                                </select>
                                            </div>

                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-5"></div>
                                            <div class="col-lg-2">
                                                <label class="form-label text-center">Accesos</label>
                                            </div>
                                            <div class="col-lg-5"></div>
                                        </div>
                                        <div class="col-lg-12" id="chk_access"></div>
                                    </form>
                                </div>
                                <!--//app-card-body-->

                            </div>
                            <!--//app-card-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal" id="Ventana_Espera">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Procesando</h4>
                    </div>
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content" id="tablausuarios">
            <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                <div class="app-card app-card-orders-table shadow-sm mb-5">
                    <div class="app-card-body">
                        <div class="table-responsive">
                            <table class="table app-table-hover mb-0 text-left">
                                <thead>
                                    <tr>
                                        <th class="cell" style="text-align: center">No Usuario</th>
                                        <th class="cell" style="text-align: left">Nombre</th>
                                        <th class="cell" style="text-align: left">Apellido Paterno</th>
                                        <th class="cell" style="text-align: left">Apellido Materno</th>
                                        <th class="cell" style="text-align: left">Email</th>
                                        <th class="cell" style="text-align: left">Estatus</th>
                                        <th class="cell" style="text-align: right">Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:ListView ID="usuarioslist" runat="server" ItemType="System.String[]"
                                        SelectMethod="GetUsuarios" ViewStateMode="Disabled">
                                        <ItemTemplate>
                                            <tr id="Producto1_<%#: Item[0] %>">
                                                <%-- <td class="ti-close1 " id="<%#: Item[0] %>">  <asp:Button CssClass="botoncito"  runat="server" ID="btnImprime"  OnClick="btnimprime_Click" value="<%#: Item[0] %>" /> </td>
                                                --%>
                                                <td class="cart-title first-row" style="text-align: center">
                                                    <input class="producto" id="Producto" value="<%#: Item[0] %>" hidden="hidden"></input>
                                                    <span><%#: Item[0] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[1] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[2] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[3] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[4] %></span>
                                                </td>
                                                <td style="text-align: left">
                                                    <span><%#: Item[5] %></span>
                                                </td>
                                                <td style="text-align: right">
                                                    <%--<input class="fas fa-edit" id="Edit" type="button" value="<%#: Item[0] %>"></input> --%>
                                                    <button type="button" class="btn btn-success btn-lg tr" value="<%#: Item[0] %>" id="<%#: Item[0] %>"><strong style="color: white"></strong><i class="fa fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <div>
                                                <p style="color:#5d6778">No hay mas Usuarios</p>
                                            </div>
                                        </EmptyDataTemplate>
                                    </asp:ListView>

                                </tbody>
                            </table>
                        </div>
                        <!--//table-responsive-->

                    </div>
                    <!--//app-card-body-->
                </div>
                <!--//app-card-->
                <%--<nav class="app-pagination">
							<ul class="pagination justify-content-center">
								<li class="page-item disabled">
									<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
							    </li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item">
								    <a class="page-link" href="#">Next</a>
								</li>
							</ul>
						</nav>--%><!--//app-pagination-->

            </div>
            <!--//tab-pane-->

            <%--   <div class="tab-pane fade" id="orders-paid" role="tabpanel" aria-labelledby="orders-paid-tab">
					    <div class="app-card app-card-orders-table mb-5">
						    <div class="app-card-body">
							    <div class="table-responsive">
								    
							        <table class="table mb-0 text-left">
										<thead>
											<tr>
												<th class="cell">Order</th>
												<th class="cell">Product</th>
												<th class="cell">Customer</th>
												<th class="cell">Date</th>
												<th class="cell">Status</th>
												<th class="cell">Total</th>
												<th class="cell"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="cell">#15346</td>
												<td class="cell"><span class="truncate">Lorem ipsum dolor sit amet eget volutpat erat</span></td>
												<td class="cell">John Sanders</td>
												<td class="cell"><span>17 Oct</span><span class="note">2:16 PM</span></td>
												<td class="cell"><span class="badge bg-success">Paid</span></td>
												<td class="cell">$259.35</td>
												<td class="cell"><a class="btn-sm app-btn-secondary" href="#">View</a></td>
											</tr>
											
											<tr>
												<td class="cell">#15344</td>
												<td class="cell"><span class="truncate">Pellentesque diam imperdiet</span></td>
												<td class="cell">Teresa Holland</td>
												<td class="cell"><span class="cell-data">16 Oct</span><span class="note">01:16 AM</span></td>
												<td class="cell"><span class="badge bg-success">Paid</span></td>
												<td class="cell">$123.00</td>
												<td class="cell"><a class="btn-sm app-btn-secondary" href="#">View</a></td>
											</tr>
											
											<tr>
												<td class="cell">#15343</td>
												<td class="cell"><span class="truncate">Vestibulum a accumsan lectus sed mollis ipsum</span></td>
												<td class="cell">Jayden Massey</td>
												<td class="cell"><span class="cell-data">15 Oct</span><span class="note">8:07 PM</span></td>
												<td class="cell"><span class="badge bg-success">Paid</span></td>
												<td class="cell">$199.00</td>
												<td class="cell"><a class="btn-sm app-btn-secondary" href="#">View</a></td>
											</tr>
										
											
											<tr>
												<td class="cell">#15341</td>
												<td class="cell"><span class="truncate">Morbi vulputate lacinia neque et sollicitudin</span></td>
												<td class="cell">Raymond Atkins</td>
												<td class="cell"><span class="cell-data">11 Oct</span><span class="note">11:18 AM</span></td>
												<td class="cell"><span class="badge bg-success">Paid</span></td>
												<td class="cell">$678.26</td>
												<td class="cell"><a class="btn-sm app-btn-secondary" href="#">View</a></td>
											</tr>
		
										</tbody>
									</table>
						        </div><!--//table-responsive-->
						    </div><!--//app-card-body-->		
						</div><!--//app-card-->
			        </div><!--//tab-pane-->
			        
			        <div class="tab-pane fade" id="orders-pending" role="tabpanel" aria-labelledby="orders-pending-tab">
					    <div class="app-card app-card-orders-table mb-5">
						    <div class="app-card-body">
							    <div class="table-responsive">
							        <table class="table mb-0 text-left">
										<thead>
											<tr>
												<th class="cell">Order</th>
												<th class="cell">Product</th>
												<th class="cell">Customer</th>
												<th class="cell">Date</th>
												<th class="cell">Status</th>
												<th class="cell">Total</th>
												<th class="cell"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="cell">#15345</td>
												<td class="cell"><span class="truncate">Consectetur adipiscing elit</span></td>
												<td class="cell">Dylan Ambrose</td>
												<td class="cell"><span class="cell-data">16 Oct</span><span class="note">03:16 AM</span></td>
												<td class="cell"><span class="badge bg-warning">Pending</span></td>
												<td class="cell">$96.20</td>
												<td class="cell"><a class="btn-sm app-btn-secondary" href="#">View</a></td>
											</tr>
										</tbody>
									</table>
						        </div><!--//table-responsive-->
						    </div><!--//app-card-body-->		
						</div><!--//app-card-->
			        </div><!--//tab-pane-->
			        <div class="tab-pane fade" id="orders-cancelled" role="tabpanel" aria-labelledby="orders-cancelled-tab">
					    <div class="app-card app-card-orders-table mb-5">
						    <div class="app-card-body">
							    <div class="table-responsive">
							        <table class="table mb-0 text-left">
										<thead>
											<tr>
												<th class="cell">No Usuario</th>
												<th class="cell">Nombre</th>
												<th class="cell">Apellido Paterno</th>
												<th class="cell">Apellido Materno</th>
												<th class="cell">Email</th>
												<th class="cell">Estatus</th>
												<th class="cell"></th>
											</tr>
										</thead>
										<tbody>
											
											<tr>
												<td class="cell">#15342</td>
												<td class="cell"><span class="truncate">Justo feugiat neque</span></td>
												<td class="cell">Reina Brooks</td>
												<td class="cell"><span class="cell-data">12 Oct</span><span class="note">04:23 PM</span></td>
												<td class="cell"><span class="badge bg-danger">Cancelled</span></td>
												<td class="cell">$59.00</td>
												<td class="cell"><a class="btn-sm app-btn-secondary" href="#">View</a></td>
											</tr>
											
										</tbody>
									</table>
						        </div><!--//table-responsive-->
						    </div><!--//app-card-body-->		
						</div><!--//app-card-->
			        </div><!--//tab-pane-->--%>
        </div>
        <!--//tab-content-->

    </div>


</asp:Content>

﻿using agroBJX.Models;
using agroBJX.Models.Assistant;
using agroBJX.Models.Deal;
using agroBJX.Sessiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX.Paginas.Catalogos
{
    
    public partial class Usuarios :Page
    {
        public Usuario ControladorUsuario = new Usuario();
        protected void Page_Load(object sender, EventArgs e)
        {
            string usuario = "";
            usuario = Cls_Sessiones.Nombre_Usuario;
            if (!String.IsNullOrEmpty(usuario))
            {
                GetUsuarios();
            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }
        public IEnumerable<string[]> GetUsuarios()
        {
            DataTable Dt_Data = new DataTable();
            Dt_Data = ControladorUsuario.Consult();

                var result = from Fila in Dt_Data.AsEnumerable()
                             select new[] {

                                     (Fila["Usuario_ID"] is DBNull ? string.Empty : Fila.Field<String>("Usuario_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila.Field<String>("Apellido_Paterno") == null ? string.Empty : Fila.Field<String>("Apellido_Paterno").Trim()),
                                     (Fila.Field<String>("Apellido_Materno") == null ? string.Empty : Fila.Field<String>("Apellido_Materno").Trim()),
                                     (Fila.Field<String>("Email") == null ? string.Empty :  Seguridad.DesEncriptar(Fila.Field<String>("Email").Trim())),
                                     (Fila.Field<String>("Estatus") == null ? string.Empty : Fila.Field<String>("Estatus").Trim()),
                                     

                             };
                return result;
            
        }
        

    }
}
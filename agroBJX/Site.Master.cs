﻿using agroBJX.Models;
using agroBJX.Sessiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace agroBJX
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        String Usuario_ID = Cls_Sessiones.No_Usuario;

        //funcion para cargar el menu de catalogos
        public IEnumerable<string[]> GetMenu()
        {

            Accesos Controlador = new Accesos();
            Controlador.Usuario_ID = Usuario_ID;
            DataTable Dt_Data = new DataTable();
            
            
                Dt_Data = Controlador.Consult();
                
                    var result = from Fila in Dt_Data.AsEnumerable()
                                 select new[] {
                                     (Fila["Usuario_ID"] is DBNull ? string.Empty : Fila.Field<string>("Usuario_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila["Controlador"] is DBNull ? string.Empty : Fila.Field<string>("Controlador").Trim())

                             };
                
           
            return result;
        }

        //funcion para cargar submenu de catalogos
        public IEnumerable<string[]> GetSubmenu()
        {

            Accesos Controlador = new Accesos();
            Controlador.Usuario_ID = Usuario_ID;
            DataTable Dt_Data = new DataTable();


            Dt_Data = Controlador.Consult2();

            var result = from Fila in Dt_Data.AsEnumerable()
                         select new[] {
                                     (Fila["Usuario_ID"] is DBNull ? string.Empty : Fila.Field<string>("Usuario_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila["Controlador"] is DBNull ? string.Empty : Fila.Field<string>("Controlador").Trim())

                             };


            return result;
        }

        //funcion para cargar menu de operaciones

        public IEnumerable<string[]> GetMenuoperaciones()
        {

            Accesos Controlador = new Accesos();
            Controlador.Usuario_ID = Usuario_ID;
            DataTable Dt_Data = new DataTable();


            Dt_Data = Controlador.Consultoperaciones();

            var result = from Fila in Dt_Data.AsEnumerable()
                         select new[] {
                                     (Fila["Usuario_ID"] is DBNull ? string.Empty : Fila.Field<string>("Usuario_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila["Controlador"] is DBNull ? string.Empty : Fila.Field<string>("Controlador").Trim())

                             };


            return result;
        }

        //funcion submenu de operaciones

        public IEnumerable<string[]> GetSubmenuoperaciones()
        {

            Accesos Controlador = new Accesos();
            Controlador.Usuario_ID = Usuario_ID;
            DataTable Dt_Data = new DataTable();


            Dt_Data = Controlador.Consultsuboperaciones();

            var result = from Fila in Dt_Data.AsEnumerable()
                         select new[] {
                                     (Fila["Usuario_ID"] is DBNull ? string.Empty : Fila.Field<string>("Usuario_ID").ToString()),
                                     (Fila.Field<String>("Nombre") == null ? string.Empty : Fila.Field<String>("Nombre").Trim()),
                                     (Fila["Controlador"] is DBNull ? string.Empty : Fila.Field<string>("Controlador").Trim())

                             };


            return result;
        }
    }
}
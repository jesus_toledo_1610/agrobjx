﻿var $ClienteID='';
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    eventos();
    cargar_clientes();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>

function eventos() {

     $(document).on('click', '.tr', function () {
        var id = $(this).attr('id');
         //var x = $(".tr").val();
         //alert(x);
         habilitar_controles('Edit')
         //alert(x);
         Cargar_Informacion(id);
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('Cancel');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');

    });

    $('#btn-save-direccion').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });
        }

    });

      $('#cmb-cliente').on("select2:select", function (evt) {
        $ClienteID = evt.params.data.id;
    });
    $("#cmb-cliente").on("select2:unselecting instead", function (e) {
        $ClienteID = '';
    });
}

/*====================================== OPERACIONES ===================================*/

//funcion para cargar los clientes al combo
function cargar_clientes() {
    jQuery('#cmb-cliente').select2({
        ajax: {
            method: 'GET',
            url: "../../api/listaclientes",
            dataType: 'json',
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ClienteID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    
                    results: JSON.parse(data),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
       // theme: "bootstrap",
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div  class='select2-result-repository__title '><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
//funcion para abrir ventana de espera
function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

//funcion para cerrar ventana de espera
function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}

//funcion para insertar o actualizar un registro
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();
        Obj_Capturado.Direccion_Envio_ID = $('#txt-DireccionID').val();
        Obj_Capturado.Cliente_ID = $('#cmb-cliente :selected').val();
        
        Obj_Capturado.Direccion_Envio = $('#txt-dire').val();
        Obj_Capturado.Colonia = $('#txt-col').val();
        Obj_Capturado.CP = $('#txt-cp').val();
        Obj_Capturado.Estado = $('#txt-estado').val();
        Obj_Capturado.Pais = $('#txt-pais').val();
        Obj_Capturado.Ciudad = $('#txt-ciudad').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();
        Obj_Capturado.Captura = $('#txt-DireccionID').val() == "" ? 'I' : 'U';
        
        $.ajax({
            type: 'POST',
            url: "../../api/direcciones",
            data: JSON.stringify(Obj_Capturado),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (Resultado) {
                
                if (Resultado == "Registro exitoso.") {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'success',
                        title: 'Registro Realizado con Exito!!',
                        timer: 2500
                    });
                    setInterval("actualizar()", 1100);
                    limpiar_controles();
                } else if (Resultado == "Registro actualizado correctamente.") {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'success',
                        title: 'Registro Actualizado con Exito!!',
                        timer: 2500
                    });
                    setInterval("actualizar()", 1100);
                    limpiar_controles();
                } else {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo Salio mal',
                        footer: Resultado
                    });
                    setInterval("actualizar()", 2100);
                    limpiar_controles();
                }

            }
        });
    } catch (e) {
        Cerrar_Ventana_Espera();
        alert(e);
       // mostrar_mensaje("Informe Técnico", e);
    }
}

function actualizar() {
    location.reload(true);
}

//funcion para validar los datos del formulario
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {


        if ($('#cmb-stt').val() == '' || $('#cmb-stt').val() == undefined || $('#cmb-stt').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Estatus</strong>.<br />';
        }
        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

//funcion para cargar informacion de las aduanas
function Cargar_Informacion(Eve_ID) {
    var Obj_Capturado = new Object();
    Obj_Capturado.Usuario_ID = Eve_ID;
    
    try {
        $.ajax({
            method: 'GET',
            url: "../../api/consultardireccion/" + Eve_ID,
            //data: JSON.stringify(Obj_Capturado),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            
            success: function (Resultado) {
                row = JSON.parse(Resultado);
                $('#txt-DireccionID').val(row[0].Direccion_Envio_ID);
                $('#txt-cp').val(row[0].CP);
                $('#txt-estado').val(row[0].Estado);
                $('#txt-ciudad').val(row[0].Ciudad);
                $('#txt-pais').val(row[0].Pais);
                $('#txt-dire').val(row[0].Direccion_Envio);
                $('#txt-col').val(row[0].Colonia);
                $('#cmb-stt').val(row[0].Estatus);
                 if (row[0].Cliente_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Cliente_ID).text(row[0].Cliente);
                    $("#cmb-cliente").append($newOption).trigger('change');   
                }
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    
    habilitar_controles("Edit");
}


/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Cancel":
            $('#btn-new').css({ display: 'Block' });
            $('#btn-emp').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
    }
}

/// FUNCION PARA LIMPIAR LOS CONTROLES
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("input[name=Menu]").each(function (index) {
        if ($(this).is(':checked')) {
            $(this).prop("checked", false)

        }
    });
    $('#txt-nombre').val('');
    $('#cmb-stt').val('');
}

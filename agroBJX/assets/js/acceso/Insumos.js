﻿var $TipoInsumoID = '';
var UnidadID = '';
var ProveedorID = '';
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    eventos();
    cargar_tipo_insumo();
    cargar_proveedores();
    cargar_tipo_unidades();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>

function eventos() {

     $(document).on('click', '.tr', function () {
        var id = $(this).attr('id');
         //var x = $(".tr").val();
         //alert(x);
         habilitar_controles('Edit')
         //alert(x);
         Cargar_Informacion(id);
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('Cancel');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');

    });

    $('#btn-save-insumo').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });
        }

    });

    $('#cmb-ti').on("select2:select", function (evt) {
        $TipoInsumoID = evt.params.data.id;
    });
    $("#cmb-ti").on("select2:unselecting instead", function (e) {
        $TipoInsumoID = '';
    });

    $('#cmb-uni').on("select2:select", function (evt) {
        $UnidadID = evt.params.data.id;
    });
    $("#cmb-uin").on("select2:unselecting instead", function (e) {
        $UnidadID = '';
    });

    $('#cmb-pro').on("select2:select", function (evt) {
        $ProveedorID = evt.params.data.id;
    });
    $("#cmb-pro").on("select2:unselecting instead", function (e) {
        $ProveedorID = '';
    });

$('#txt-IVA').change(function (e) {
    calcular_impuestos();
});
$('#txt-IEPS').change(function (e) {
    calcular_impuestos();
});
$('#txt-costo').change(function (e) {
    calcular_impuestos();
});
}

/*====================================== OPERACIONES ===================================*/

function calcular_impuestos() {
    var porcentaje_iva =  0;
    var porcentaje_ieps = 0;
    var iva = 0;
    var ieps = 0;
    var total = 0;
    var costo = 0;

    costo = $("#txt-costo").val();
    porcentaje_ieps = $('#txt-IEPS').val();
    porcentaje_iva = $('#txt-IVA').val();
    

    if (porcentaje_iva > 0) {
        iva = porcentaje_iva;
        iva = iva;
    } else {

        iva = iva;
    }
    if (porcentaje_ieps > 0) {
        ieps = porcentaje_ieps;
        ieps = ieps;
    } else {

        ieps = ieps;
    }
    
    total = parseFloat(costo) + parseFloat(iva) + parseFloat(ieps );
    total=total.toFixed(2);
    $("#txt-total").val(total);
}
//funcion para cargar los tipos de insumo
function cargar_tipo_insumo() {
    jQuery('#cmb-ti').select2({
        ajax: {
            method: 'GET',
            url: "../../api/listatipoinsumos",
            dataType: 'json',
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $TipoInsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    
                    results: JSON.parse(data),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
       // theme: "bootstrap",
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div  class='select2-result-repository__title '><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

//funcion para cargar los tipos de unidades
function cargar_tipo_unidades() {
    jQuery('#cmb-uni').select2({
        ajax: {
            method: 'GET',
            url: "../../api/listaunidades",
            dataType: 'json',
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $TipoInsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {

                    results: JSON.parse(data),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        // theme: "bootstrap",
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div  class='select2-result-repository__title '><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

//funcion para cargar los proveedores
function cargar_proveedores() {
    jQuery('#cmb-pro').select2({
        ajax: {
            method: 'GET',
            url: "../../api/listaproveedores",
            dataType: 'json',
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $TipoInsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {

                    results: JSON.parse(data),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        // theme: "bootstrap",
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div  class='select2-result-repository__title '><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
//funcion para abrir ventana de espera
function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

//funcion para cerrar ventana de espera
function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}

//funcion para insertar o actualizar un registro
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();
        Obj_Capturado.Insumo_ID = $('#txt-InsumoID').val();
        Obj_Capturado.Nombre = $('#txt-nombre').val();
        Obj_Capturado.Unidad_ID = $('#cmb-uni :selected').val();
        Obj_Capturado.Tipo_Insumo_ID = $('#cmb-ti :selected').val();
        Obj_Capturado.Proveedor_ID = $('#cmb-pro :selected').val();
        Obj_Capturado.Costo = $('#txt-costo').val();
        Obj_Capturado.IEPS = $('#txt-IEPS').val();
        Obj_Capturado.IVA = $('#txt-IVA').val();
        Obj_Capturado.Total = $('#txt-total').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();
        Obj_Capturado.Captura = $('#txt-InsumoID').val() == "" ? 'I' : 'U';
        
        $.ajax({
            type: 'POST',
            url: "../../api/insumos",
            //data: JSON.stringify(Obj_Capturado),
            data: JSON.stringify(Obj_Capturado),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            //  data: "{'Datos':'" + JSON.stringify(Productos) + "'Cupon':'" + JSON.stringify(Obj_Capturado) "'}",
            success: function (Resultado) {
                
                if (Resultado == "Registro exitoso.") {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'success',
                        title: 'Registro Realizado con Exito!!',
                        timer: 2500
                    });
                    setInterval("actualizar()", 1100);
                    limpiar_controles();
                } else if (Resultado == "Registro actualizado correctamente.") {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'success',
                        title: 'Registro Actualizado con Exito!!',
                        timer: 2500
                    });
                    setInterval("actualizar()", 1100);
                    limpiar_controles();
                } else {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo Salio mal',
                        footer: Resultado
                    });
                    setInterval("actualizar()", 2100);
                    limpiar_controles();
                }

            }
        });
    } catch (e) {
        Cerrar_Ventana_Espera();
        alert(e);
       // mostrar_mensaje("Informe Técnico", e);
    }
}

function actualizar() {
    location.reload(true);
}

//funcion para validar los datos del formulario
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {

        if ($('#txt-nombre').val() == '' || $('#txt-nombre').val() == undefined || $('#txt-nombre').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong><span class="fas fa-angle-right"> </span> Nombre </strong><br />';
        }
        if ($('#cmb-uni').val() == '' || $('#cmb-uni').val() == undefined || $('#cmb-uni').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Unidad</strong>.<br />';
        }
        if ($('#cmb-ti').val() == '' || $('#cmb-ti').val() == undefined || $('#cmb-ti').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Tipo Insumo</strong>.<br />';
        }
        if ($('#cmb-pro').val() == '' || $('#cmb-pro').val() == undefined || $('#cmb-pro').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Proveedor</strong>.<br />';
        }
        if ($('#txt-costo').val() == '' || $('#txt-costo').val() == undefined || $('#txt-costo').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong><span class="fas fa-angle-right"> </span> Costo</strong><br />';
        }
        if ($('#txt-IVA').val() == '' || $('#txt-IVA').val() == undefined || $('#txt-IVA').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong><span class="fas fa-angle-right"> </span> IVA </strong><br />';
        }
        if ($('#txt-IEPS').val() == '' || $('#txt-IEPS').val() == undefined || $('#txt-IEPS').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong><span class="fas fa-angle-right"> </span> IEPS </strong><br />';
        }
        if ($('#txt-total').val() == '' || $('#txt-total').val() == undefined || $('#txt-total').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong><span class="fas fa-angle-right"> </span> Total </strong><br />';
        }
        if ($('#cmb-stt').val() == '' || $('#cmb-stt').val() == undefined || $('#cmb-stt').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Estatus</strong>.<br />';
        }

        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

//funcion para cargar informacion del isumo
function Cargar_Informacion(Eve_ID) {
    var Obj_Capturado = new Object();
    Obj_Capturado.Usuario_ID = Eve_ID;
    
    try {
        $.ajax({
            method: 'GET',
            url: "../../api/consultarinsumo/" + Eve_ID,
            //data: JSON.stringify(Obj_Capturado),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            
            success: function (Resultado) {
                row = JSON.parse(Resultado);
                $('#txt-InsumoID').val(row[0].Insumo_ID);
                $('#txt-costo').val(row[0].Costo);
                $('#txt-IVA').val(row[0].IVA);
                $('#txt-IEPS').val(row[0].IEPS);
                $('#txt-nombre').val(row[0].Nombre);
                $('#txt-total').val(row[0].Total);
                $('#cmb-stt').val(row[0].Estatus);
                if (row[0].Unidad_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Unidad_ID).text(row[0].Unidad);
                    $("#cmb-uni").append($newOption).trigger('change');   
                }
                if (row[0].Tipo_Insumo_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Tipo_Insumo_ID).text(row[0].TipoInsumo);
                    $("#cmb-ti").append($newOption).trigger('change');
                }
                if (row[0].Proveedor_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Proveedor_ID).text(row[0].Proveedor);
                    $("#cmb-pro").append($newOption).trigger('change');
                }
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    
    habilitar_controles("Edit");
}


/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Cancel":
            $('#btn-new').css({ display: 'Block' });
            $('#btn-emp').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
    }
}

/// FUNCION PARA LIMPIAR LOS CONTROLES
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("input[name=Menu]").each(function (index) {
        if ($(this).is(':checked')) {
            $(this).prop("checked", false)

        }
    });
    $('#txt-nombre').val('');
    $('#cmb-stt').val('');
}

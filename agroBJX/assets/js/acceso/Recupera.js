﻿
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Clientes');
    eventos();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>

function eventos() {

    $('#btnrecupera').click(function (e) {
        e.preventDefault();
     Recupera();
    });

}

/*====================================== OPERACIONES ===================================*/
function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

//funcion para cerrar ventana de espera
function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}
//funcion para recuperar password
function Recupera() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();
        Obj_Capturado.Email = $('#signin-email').val();
        
        $.ajax({
            type: 'POST',
            url: "api/recupera",
            //data: JSON.stringify(Obj_Capturado),
            data: JSON.stringify(Obj_Capturado),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            //  data: "{'Datos':'" + JSON.stringify(Productos) + "'Cupon':'" + JSON.stringify(Obj_Capturado) "'}",
            success: function (Resultado) {
                
                if (Resultado == "Registro exitoso.") {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'success',
                        title: 'Tus datos fueron enviados al correo',
                        timer: 6500
                    });
                 window.location.href = "Login.aspx";
                    
                } else if (Resultado == "Registro actualizado correctamente.") {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'success',
                        title: 'Registro Actualizado con Exito!!',
                        timer: 2500
                    });
                    setInterval("actualizar()", 1100);
                    limpiar_controles();
                } else {
                    Cerrar_Ventana_Espera();
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo Salio mal',
                        footer: Resultado
                    });
                    setInterval("actualizar()", 2100);
                    limpiar_controles();
                }

            }
        });
    } catch (e) {
        Cerrar_Ventana_Espera();
        alert(e);
       // mostrar_mensaje("Informe Técnico", e);
    }
}

function actualizar() {
    location.reload(true);
}

//funcion para validar los datos del formulario
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {

        
        if ($('#txt-email').val() == '' || $('#txt-email').val() == undefined || $('#txt-email').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Correo Electronico</strong>.<br />';
        }
        if ($('#txt-razon').val() == '' || $('#txt-razon').val() == undefined || $('#txt-razon').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Razon Social</strong>.<br />';
        }
        if ($('#txt-rfc').val() == '' || $('#txt-rfc').val() == undefined || $('#txt-rfc').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> RFC</strong>.<br />';
        }
        if ($('#txt-tel').val() == '' || $('#txt-tel').val() == undefined || $('#txt-tel').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Telefono</strong>.<br />';
        }
        if ($('#txt-dir').val() == '' || $('#txt-dir').val() == undefined || $('#txt-dir').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Calle</strong>.<br />';
        }
        if ($('#txt-ne').val() == '' || $('#txt-ne').val() == undefined || $('#txt-ne').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Numero Exterior</strong>.<br />';
        }
        if ($('#txt-col').val() == '' || $('#txt-col').val() == undefined || $('#txt-col').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Colonia</strong>.<br />';
        }
        if ($('#txt-cp').val() == '' || $('#txt-cp').val() == undefined || $('#txt-cp').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> C.P</strong>.<br />';
        }
        if ($('#txt-ciudad').val() == '' || $('#txt-ciudad').val() == undefined || $('#txt-ciudad').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Ciudad</strong>.<br />';
        }
        if ($('#txt-estado').val() == '' || $('#txt-estado').val() == undefined || $('#txt-estado').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Estado</strong>.<br />';
        }
        if ($('#txt-pais').val() == '' || $('#txt-pais').val() == undefined || $('#txt-pais').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Pais</strong>.<br />';
        }
        if ($('#txt-nombre').val() == '' || $('#txt-nombre').val() == undefined || $('#txt-nombre').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Contacto</strong>.<br />';
        }
       
        if ($('#cmb-stt').val() == '' || $('#cmb-stt').val() == undefined || $('#cmb-stt').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Estatus</strong>.<br />';
        }

        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

//funcion para cargar informacion del cliente
function Cargar_Informacion(Eve_ID) {
    var Obj_Capturado = new Object();
    Obj_Capturado.Usuario_ID = Eve_ID;
    
    try {
        $.ajax({
            method: 'GET',
            url: "../../api/consultarcliente/" + Eve_ID,
            //data: JSON.stringify(Obj_Capturado),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            
            success: function (Resultado) {
                row = JSON.parse(Resultado);
                $('#txt-ClienteID').val(row[0].Cliente_ID);
                $('#txt-nombre').val(row[0].Nombre);
                $('#txt-tel').val(row[0].Telefono);
                $('#txt-email').val(row[0].Email);
                $('#txt-razon').val(row[0].Razon_Social);
                $('#txt-rfc').val(row[0].RFC);
                $('#txt-dc').val(row[0].Dias_Credito);
                $('#txt-lc').val(row[0].Limite_Credito);
                $('#txt-dir').val(row[0].Direccion);
                $('#txt-col').val(row[0].Colonia);
                $('#txt-ne').val(row[0].Numero_E);
                $('#txt-ni').val(row[0].Numero_I);
                $('#txt-cp').val(row[0].C_P);
                $('#txt-ciudad').val(row[0].Ciudad);
                $('#txt-estado').val(row[0].Estado);
                $('#txt-pais').val(row[0].Pais);
                $('#cmb-stt').val(row[0].Estatus);;
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    
    habilitar_controles("Edit");
}


/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Cancel":
            $('#btn-new').css({ display: 'Block' });
            $('#btn-emp').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
    }
}

/// FUNCION PARA LIMPIAR LOS CONTROLES
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("input[name=Menu]").each(function (index) {
        if ($(this).is(':checked')) {
            $(this).prop("checked", false)

        }
    });
    $('#txt-nombre').val('');
    $('#txt-app').val('');
    $('#txt-apm').val('');
    $('#txt-email').val('');
    $('#txt-pass').val('');
    $('#cmb-stt').val('');
}

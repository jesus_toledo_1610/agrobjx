﻿/*====================================== VARIABLES =====================================*/
var $Correo = '';
var $EmpleadoID = '';
var $UnidadNegocioID = '';
var $EmpresaID = '';
var oTable;

var dataSet = [{ Menu: "Usuarios", Etiqueta: "Usuarios" },
    { Menu: "Clientes", Etiqueta: "Clientes" },
    { Menu: "Proveedores", Etiqueta: "Proveedores" },
    { Menu: "Fleteras", Etiqueta: "Fleteras" },
    { Menu: "Aduanas", Etiqueta: "Aduanas" },
    { Menu: "Tipo Insumos", Etiqueta: "Tipo Insumos" },
    { Menu: "Insumos", Etiqueta: "Insumos" },
    { Menu: "Productos", Etiqueta: "Productos" },
    { Menu: "Unidades", Etiqueta: "Unidades" },
    { Menu: "Notas Embarque", Etiqueta: "Notas_Embarque" },
    { Menu: "Direcciones Envio", Etiqueta: "Direcciones_Envio" },
];


/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Usuarios');
    eventos();
    
    cargar_tabla_accesos();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>

function eventos() {

     $(document).on('click', '.tr', function () {
        var id = $(this).attr('id');
         //var x = $(".tr").val();
         //alert(x);
         habilitar_controles('Edit')
         //alert(x);
         Cargar_Informacion(id);
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('Cancel');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');

    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {

            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });
        }

    });

    $('#txt-email').change(function (e) {
        e.preventDefault();
        password();
    });
}

/*====================================== OPERACIONES ===================================*/
//funcion para abrir ventana de espera
function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

//funcion para cerrar ventana de espera
function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}

//funcion para insertar o actualizar un registro
function OperationMaster() {
    var Obj_Capturado = new Object();
  
 
    try {
        Abrir_Ventana_Espera();
        Obj_Capturado.Usuario_ID = $('#txt-usuarioid').val();
        Obj_Capturado.Nombre = $('#txt-nombre').val();
        Obj_Capturado.Apellido_Materno = $('#txt-apm').val();
        Obj_Capturado.Apellido_Paterno = $('#txt-app').val();
        Obj_Capturado.Password = $('#txt-pass').val();
        Obj_Capturado.Email = $('#txt-email').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();
        Obj_Capturado.Captura = $('#txt-usuarioid').val() == "" ? 'I' : 'U';
        
        $.ajax({
            type: 'POST',
            url: "../../api/usuarios",
            //data: JSON.stringify(Obj_Capturado),
            data: JSON.stringify(Obj_Capturado),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            //  data: "{'Datos':'" + JSON.stringify(Productos) + "'Cupon':'" + JSON.stringify(Obj_Capturado) "'}",
            success: function (Resultado) {
                var row = JSON.parse(Resultado);
                if (row.Estatus) {
                    guardaraccesos();
                   // Cerrar_Ventana_Espera();
                    //location.reload
                    //limpiar_controles();
                }

            }
        });
    } catch (e) {
        Cerrar_Ventana_Espera();
        alert(e);
       // mostrar_mensaje("Informe Técnico", e);
    }
}


//funcion para generar passw0rd aleatorio
function password() {
    characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var length = 6;
    var pass = "";
    for (i = 0; i < length; i++) {
     
            pass += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    $('#txt-pass').val(pass);
}
//funcion para guardar accesos al sistema
function guardaraccesos(Array_accesos) {
    var Array_acceso = new Array();
    var Obj_accesos = new Object();
    var Contador = 0;
    if ($('#chk_Usuarios').prop('checked') || $('#chk_Clientes').prop('checked') || $('#chk_Proveedores').prop('checked') || $('#chk_Fleteras').prop('checked') || $('#chk_Aduanas').prop('checked') || $('#chk_TipoInsumos').prop('checked') || $('#chk_Insumos').prop('checked') || $('#chk_Unidades').prop('checked') || $('#chk_Productos').prop('checked') || $('#chk_Direcciones_Envio').prop('checked')) {
        Obj_accesos = new Object();
        Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
        Obj_accesos.MenuID = "1";
        Obj_accesos.Nombre = "Catálogos";
        Obj_accesos.ParentID = "1";
        Array_acceso[Contador] = Obj_accesos;
        Contador++;
    }

   if ($('#chk_Notas_Embarque').prop('checked') ) {
        Obj_accesos = new Object();
        Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
        Obj_accesos.MenuID = "2";
        Obj_accesos.Nombre = "Operaciones";
        Obj_accesos.ParentID = "2";
        Array_acceso[Contador] = Obj_accesos;
        Contador++;
    }

    $("input[name=Menu]").each(function (index) {
        if ($(this).is(':checked')) {
            Obj_accesos = new Object();

            if ($(this).val() == "Usuarios") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Usuarios";
                Obj_accesos.Controlador = "Paginas/Catalogos/Usuarios.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Usuarios.aspx";
                Obj_accesos.ParentID = "3";
                Array_acceso[Contador] = Obj_accesos;
            }
            if ($(this).val() == "Unidades") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Unidades";
                Obj_accesos.Controlador = "Paginas/Catalogos/Unidades.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Unidades.aspx";
                Obj_accesos.ParentID = "4";
                Array_acceso[Contador] = Obj_accesos;
            }
            if ($(this).val() == "Tipo Insumos") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Tipo Insumos";
                Obj_accesos.Controlador = "Paginas/Catalogos/TipoInsumos.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/TipoInsumos.aspx";
                Obj_accesos.ParentID = "5";
                Array_acceso[Contador] = Obj_accesos;
            }
            if ($(this).val() == "Clientes") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Clientes";
                Obj_accesos.Controlador = "Paginas/Catalogos/Clientes.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Clientes.aspx";
                Obj_accesos.ParentID = "6";
                Array_acceso[Contador] = Obj_accesos;
            }
            if ($(this).val() == "Proveedores") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Proveedores";
                Obj_accesos.Controlador = "Paginas/Catalogos/Proveedores.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Proveedores.aspx";
                Obj_accesos.ParentID = "7";
                Array_acceso[Contador] = Obj_accesos;
            }
            if ($(this).val() == "Fleteras") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Fleteras";
                Obj_accesos.Controlador = "Paginas/Catalogos/Fleteras.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Fleteras.aspx";
                Obj_accesos.ParentID = "8";
                Array_acceso[Contador] = Obj_accesos;
            }
            if ($(this).val() == "Aduanas") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Aduanas";
                Obj_accesos.Controlador = "Paginas/Catalogos/Aduanas.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Aduanas.aspx";
                Obj_accesos.ParentID = "9";
                Array_acceso[Contador] = Obj_accesos;
            }
          

            if ($(this).val() == "Insumos") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Insumos";
                Obj_accesos.Controlador = "Paginas/Catalogos/Insumos.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Insumos.aspx";
                Obj_accesos.ParentID = "10";
                Array_acceso[Contador] = Obj_accesos;
            }
          
            if ($(this).val() == "Productos") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Productos";
                Obj_accesos.Controlador = "Paginas/Catalogos/Productos.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/Productos.aspx";
                Obj_accesos.ParentID = "11";
                Array_acceso[Contador] = Obj_accesos;
            }
                
             if ($(this).val() == "Direcciones Envio") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "1";
                Obj_accesos.Nombre = "Direcciones Envio";
                Obj_accesos.Controlador = "Paginas/Catalogos/DireccionesEnvio.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/DireccionesEnvio.aspx";
                Obj_accesos.ParentID = "12";
                Array_acceso[Contador] = Obj_accesos;
            }

              if ($(this).val() == "Notas Embarque") {
                Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();
                Obj_accesos.MenuID = "2";
                Obj_accesos.Nombre = "Notas Embarque";
                Obj_accesos.Controlador = "Paginas/Catalogos/NotasEmbarque.aspx";
                Obj_accesos.Accion = "Paginas/Catalogos/NotasEmbarque.aspx";
                Obj_accesos.ParentID = "13";
                Array_acceso[Contador] = Obj_accesos;
            } 

            Contador++;
        }

    });
    $.ajax({
        type: 'POST',
        url: "../../api/accesos",
        data: JSON.stringify(Array_acceso),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        //  data: "{'Datos':'" + JSON.stringify(Productos) + "'Cupon':'" + JSON.stringify(Obj_Capturado) "'}",
        success: function (Resultado) {
            var row = JSON.parse(Resultado);                
            Cerrar_Ventana_Espera();
            Swal.fire({
                type: 'success',
                title: 'Registro Realizado con Exito!!',
                timer: 2500
            });
            setInterval("actualizar()", 1100);
                limpiar_controles();
            

        }
    });
    
}
function actualizar() {
    location.reload(true);
}
//funcion para cargar los accesos al sistema
function cargar_accesos(Eve_ID) {
    var Obj_accesos = new Object();
    var registros = "{}";
    try {
        Obj_accesos.Usuario_ID = $('#txt-usuarioid').val();;
        $.ajax({
            type: 'GET',
            url: "../../api/consultaaccesos/" + Eve_ID,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_accesos),
            cache: false,
            success: function (Resultado) {
                
                    registros = JSON.parse(Resultado);
                    $.each(registros, function (i, item) {
                        $("input[name=Menu]").each(function (index) {
                            if ($(this).val() == item.Nombre) {
                                $(this).prop('checked', true);
                            }
                        });
                    });
                
            }
        });
    } catch (e) {
        asignar_modal("Informe Técnico", e);
        jQuery('#modal_mensaje').modal({ backdrop: 'static', keyboard: false });
    }
}

//funcion para generar los check del acceso al sitema
function cargar_tabla_accesos() {
    var txt = "";
    jQuery.each(dataSet, function (index, value) {
        txt += "<div class='row'>";
        txt += "<div class='col-lg-3'>";
        txt += "<label class='form-label ' style='font-size:15px; '>" + value.Menu + "</label>";
        txt += "</div>";
        txt += "<div class='col-lg-3'>";
        txt += "<div class='form-check form-switch'>";
        txt += "<input class='form-check-input' type='checkbox' id='chk_" + value.Etiqueta + "' name='Menu' value='" + value.Menu + "'/>";
        txt += "<label for='chk_" + value.Etiqueta + "' class='badge-success'></label>";
        txt += "</div>";
        txt += "</div>";
        txt += "</div>";
    });
    $("#chk_access").append(txt);
}

//funcion para validar los datos del formulario
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {

        if ($('#txt-nombre').val() == '' || $('#txt-nombre').val() == undefined || $('#txt-nombre').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong><span class="fas fa-angle-right"> </span> Nombre </strong><br />';
        }
        if ($('#txt-email').val() == '' || $('#txt-email').val() == undefined || $('#txt-email').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Correo Electronico</strong>.<br />';
        }

        if ($('#txt-pass').val() == '' || $('#txt-pass').val() == undefined || $('#txt-pass').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Se necesita una contraseña</strong>.<br />';
        }
        if ($('#txt-app').val() == '' || $('#txt-app').val() == undefined || $('#txt-app').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Se necesita el Apellido Paterno</strong>.<br />';
        }
        if ($('#txt-apm').val() == '' || $('#txt-apm').val() == undefined || $('#txt-apm').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<strong> <span class="fas fa-angle-right"> </span> Se necesita el Apellido Materno</strong>.<br />';
        }
        if ($('#cmb-stt').val() == '' || $('#cmb-stt').val() == undefined || $('#cmb-stt').val() == null) {
            output.Estatus = false;
            output.Mensaje += ' <strong> <span class="fas fa-angle-right"></span> Estatus</strong>.<br />';
        }

        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

//funcion para cargar informacion del usuario
function Cargar_Informacion(Eve_ID) {
    var Obj_Capturado = new Object();
    Obj_Capturado.Usuario_ID = Eve_ID;
    
    try {
        $.ajax({
            method: 'GET',
            url: "../../api/consultar/" + Eve_ID,
            //data: JSON.stringify(Obj_Capturado),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            
            success: function (Resultado) {
                row = JSON.parse(Resultado);
                $('#txt-usuarioid').val(row[0].Usuario_ID);
                $('#txt-nombre').val(row[0].Nombre);
                $('#txt-app').val(row[0].Apellido_Paterno);
                $('#txt-apm').val(row[0].Apellido_Materno);
                $('#txt-email').val(row[0].Email);
                $('#txt-pass').val(row[0].Password);
                $('#cmb-stt').val(row[0].Estatus);;
                cargar_accesos(Eve_ID);

            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    
    habilitar_controles("Edit");
}


/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-emp').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#tablausuarios").css({ display: 'none' });
            break;
        case "Cancel":
            $('#btn-new').css({ display: 'Block' });
            $('#btn-emp').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#tablausuarios").css({ display: 'Block' });
            //cargar_tabla();
            break;
    }
}

/// FUNCION PARA LIMPIAR LOS CONTROLES
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("input[name=Menu]").each(function (index) {
        if ($(this).is(':checked')) {
            $(this).prop("checked", false)

        }
    });
    $('#txt-nombre').val('');
    $('#txt-app').val('');
    $('#txt-apm').val('');
    $('#txt-email').val('');
    $('#txt-pass').val('');
    $('#cmb-stt').val('');
}

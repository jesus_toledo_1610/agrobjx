$(function ($) {
    $("#div_error_acceso").css('visibility', 'hidden');
    $(".fa-3x").css('visibility', 'hidden');
    
    $('#frmLogin').submit(function () {
    // Cuando envia desde formulário
        event.preventDefault();
        $(".fa-3x").css('visibility', 'visible');
        var objLogin = new Object();
        objLogin.email = $("#signin-email").val();
        objLogin.password = $("#signin-password").val();

        //api/Acceso
        $.ajax({
            type: "POST",
            url: "api/Acceso",
            data: JSON.stringify(objLogin),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $(".fa-3x").css('visibility', 'hidden');
                if (response == 'Bienvenido')
                    window.location.href = "Paginas/Catalogos/Principal.aspx";
                   // window.location.href = "Paginas/Catalogos/Default.aspx";
                else
                {
                    $("#msjLogin").html(response);
                    $("#div_error_acceso").css('visibility', 'visible');

                    setTimeout(function () { $("#div_error_acceso").css('visibility', 'hidden'); },3000);
                }            
            },
            error: function (result) {
                alert('Ocurrio un Error!' + result.responseText);
                $(".fa-3x").css('visibility', 'hidden');
            }
        });
        
    });
});

$('#cssn').click(function () {

    //api/Salir
    $.ajax({
        type: "POST",
        url: "/api/Salir",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            window.location.href = "/Login.aspx";
        },
        error: function (result) {
            alert('Ocurrio un Error!' + result.responseText);
        }
    });
});
+function($, window, document, undefined){  
  var wrapSelector = ".toast-wrap";
  var toastSelector = ".toast-content";
  var toastStyles = ".toast-style";  
  var styles = '.toast-wrap{position:fixed;top:0;left:0;right:0;bottom:0;z-index:9999;margin:auto;background:rgba(0,0,0,.2);}.toast-content{position:absolute;top: 50%;left: 50%;-webkit-transform: translate(-50%,-50%);-moz-transform: translate(-50%,-50%);-ms-transform: translate(-50%,-50%);-o-transform: translate(-50%,-50%);transform: translate(-50%,-50%);padding: 10px;background:rgba(0,0,0,.7);color:#fff;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;max-width: 300px;min-width: 54px;text-align:center;cursor: default;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}.toast-img{display:block;max-width: 35px;max-height: 35px;margin: 0 auto 5px;}.success{display:block;width: 12px;height: 20px;border-right:6px solid #fff;border-bottom: 8px solid #fff;transform: rotate(45deg);-webkit-transform:rotate(45deg);margin: 0 auto 5px;}.error{display:block;width: 24px;height: 24px;font-size: 20px;color:rgba(255,255,255,.8);border:2px solid rgba(255,255,255,.8);border-radius: 50%;-webkit-border-radius: 50%;margin: 0 auto 5px;line-height:20px;text-align: center}.loading{position:relative;margin: 0 auto 5px;display:block;width:20px;height: 20px;border: 2px solid #fff;border-radius: 50%;-webkit-border-radius: 50%;animation: loading 1s linear infinite;}.loading:before{content: "";display: block;position: absolute;top: -5px;left: 0;width: 10px;height: 10px;background: #fff;border-radius: 50%; }@keyframes loading{0%{transform: rotate(0deg);}50%{transform: rotate(180deg);}100%{transform: rotate(360deg);}}@-webkit-keyframes loading{0%{-webkit-transform: rotate(0deg);}50%{-webkit-transform: rotate(180deg);}100%{-webkit-transform: rotate(360deg);}}';

  var Toast = {
      default: {
          "id": "1",
          "title": "Loading...",
          "icon": "loading",
          "image": "",
          "duration": 1500,
      },
    showToast: function(opt){
      var _this = this;
      this.options = $.extend({}, this.default, opt);
      $("<style id='toast-style-" + this.options.id + "' class:'toast-style'></style>").text(styles).appendTo($(wrapSelector));
      if(this.options.image !== ""){
          $(toastSelector).append('<img src="' + this.options.image + '" class="toast-img" alt="toast...">');
      } else {
          $(toastSelector).append('<span id="toast-icon-' + this.options.id + '" class="toast-icon"></span>');
        switch(this.options.icon){
          case "success":
            $(".toast-icon").addClass('success');
            break;
          case "error":
            $(".toast-icon").addClass('error');
            $(".toast-icon").html("&times;");
            break;
          case "none":
            $(".toast-icon").remove();
            break;
          default:
            $(".toast-icon").addClass('loading');
            break;
        }
      }
      $(toastSelector).append('<p id="toast-text-' + this.options.id + '" style="margin:0;">' + this.options.title + '</p>');
      if(this.options.duration > 0){
        setTimeout(function(){
            _this.hideToast(this.options.id);
        }, this.options.duration);
      }
    },
    hideToast: function (id) {
        $("#toast-style-" + id).fadeOut(500);
        setTimeout(function () {
            $("#toast-style-" + id).empty().remove();
            $("#toast-text-" + id).empty().remove();
            $("#toast-icon-" + id).empty().remove();
        }, 1000);
    }
  };
  $.Toast = Toast;
}(jQuery, window, document);
